#include "json_layouts.h"

namespace my_dnd {

/// When adding an attribute, this is one of 3 places.
/// The others
/// \ref Race_attributes
/// \ref mydnd_character::Race_information (sets)
std::vector<Attribute_layout<Race_attributes>> get_race_json_layout()
{

    std::vector<Attribute_layout<Race_attributes>> temp {
        {.which_attrib = Race_attributes::race_attrib,
         .json_key = "race",
         .value_type = 's'},
        {.which_attrib = Race_attributes::favored_attrib,
         .json_key = "favored",
         .value_type = 's'},
        {.which_attrib = Race_attributes::addl_lang_attrib,
         .json_key = "addl_lang",
         .value_type = 's'},
        {.which_attrib = Race_attributes::creat_size_attrib,
         .json_key = "creat_size",
         .value_type = 's'},
        {.which_attrib = Race_attributes::str_adj_attrib,
         .json_key = "str_adj",
         .value_type = 'i'},
        {.which_attrib = Race_attributes::dex_adj_attrib,
         .json_key = "dex_adj",
         .value_type = 'i'},
        {.which_attrib = Race_attributes::con_adj_attrib,
         .json_key = "con_adj",
         .value_type = 'i'},
        {.which_attrib = Race_attributes::itl_adj_attrib,
         .json_key = "itl_adj",
         .value_type = 'i'},
        {.which_attrib = Race_attributes::wis_adj_attrib,
         .json_key = "wis_adj",
         .value_type = 'i'},
        {.which_attrib = Race_attributes::cha_adj_attrib,
         .json_key = "cha_adj",
         .value_type = 'i'},
        {.which_attrib = Race_attributes::land_speed_attrib,
         .json_key = "land_speed",
         .value_type = 'i'},
        {.which_attrib = Race_attributes::extra_feat_attrib,
         .json_key = "extra_feat",
         .value_type = 'b'},
        {.which_attrib = Race_attributes::no_armor_speed_reduction_attrib,
         .json_key = "no_armor_speed_reduction",
         .value_type = 'b'},
        {.which_attrib = Race_attributes::darkvision_attrib,
         .json_key = "darkvision",
         .value_type = 'b'},
        {.which_attrib = Race_attributes::stonecunning_attrib,
         .json_key = "stonecunning",
         .value_type = 'b'},
        {.which_attrib = Race_attributes::low_light_vision_attrib,
         .json_key = "low_light_vision",
         .value_type = 'b'},
        {.which_attrib = Race_attributes::small_size_bonus_attrib,
         .json_key = "small_size_bonus",
         .value_type = 'b'},
        {.which_attrib = Race_attributes::weap_fam_attrib,
         .json_key = "weapon_familiarity",
         .value_type = 'v'},
        {.which_attrib = Race_attributes::creat_bonus_attrib,
         .json_key = "creature_bonus",
         .value_type = 'v'},
        {.which_attrib = Race_attributes::giant_bonus_attrib,
         .json_key = "giant_bonus",
         .value_type = 'b'},
        {.which_attrib = Race_attributes::stability_bonus_attrib,
         .json_key = "stability_bonus",
         .value_type = 'b'},
        {.which_attrib = Race_attributes::personality_attrib,
         .json_key = "personality",
         .value_type = 'v'},
        {.which_attrib = Race_attributes::personality_brief_attrib,
         .json_key = "personality_brief",
         .value_type = 'v'}

    };

    return temp;
}

/// When adding an attribute, this is one of 3 places.
/// The others
/// \ref Class_attributes
/// \ref mydnd_character::Class_information (sets)
std::vector<Attribute_layout<Class_attributes>> get_class_json_layout()
{
    std::vector<Attribute_layout<Class_attributes>> temp {
        {.which_attrib = Class_attributes::class_attrib,
         .json_key = "char_class",
         .value_type = 's'},
        {.which_attrib = Class_attributes::class_description_attrib,
         .json_key = "char_class_description",
         .value_type = 'v'},
        {.which_attrib = Class_attributes::class_brief_attrib,
         .json_key = "char_class_description",
         .value_type = 'v'},
        {.which_attrib = Class_attributes::align_restriction_attrib,
         .json_key = "alignment_restriction",
         .value_type = 's'},
        {.which_attrib = Class_attributes::hit_die_attrib,
         .json_key = "hit_die",
         .value_type = 's'},
        {.which_attrib = Class_attributes::deity_restriction_attrib,
         .json_key = "deity_restriction",
         .value_type = 'b'},
        {.which_attrib = Class_attributes::damage_reduction_attrib,
         .json_key = "Damage reduction",
         .value_type = 's'},
        {.which_attrib = Class_attributes::base_skills_points_attrib,
         .json_key = "skill_points_per_level",
         .value_type = 'i'},
        {.which_attrib = Class_attributes::bonus_feat_attrib,
         .json_key = "bonus_feat",
         .value_type = 'b'},
        {.which_attrib = Class_attributes::bonus_feat_level_up_pattern_attrib,
         .json_key = "bonus_feat_level_up_pattern",
         .value_type = 'v'},
        {.which_attrib = Class_attributes::bonus_feat_limit_attrib,
         .json_key = "bonus_feat_limit",
         .value_type = 'v'}

    };

    return temp;
}

/// Layout of player character files
/// \ref Attribute_layout
/// \ref Character_attributes (sets)
/// \returns vector of Character_attributes
/// \note Abilities are not here as read in special
std::vector<Attribute_layout<Character_attributes>> get_player_character_json_layout()
{
    std::vector<Attribute_layout<Character_attributes>> temp {
        {.which_attrib = Character_attributes::age_char,
         .json_key = "age",
         .value_type = 'i'},
        {.which_attrib = Character_attributes::alignment_char,
         .json_key = "alignment",
         .value_type = 's'},
        {.which_attrib = Character_attributes::deity_char,
         .json_key = "deity",
         .value_type = 's'},
        {.which_attrib = Character_attributes::gender_char,
         .json_key = "gender",
         .value_type = 's'},
        {.which_attrib = Character_attributes::height_char,
         .json_key = "height",
         .value_type = 'i'},
        {.which_attrib = Character_attributes::level_char,
         .json_key = "level",
         .value_type = 'i'},
        {.which_attrib = Character_attributes::race_char,
         .json_key = "race",
         .value_type = 's'},
        {.which_attrib = Character_attributes::class_type_char,
         .json_key = "rate",
         .value_type = 's'},
        {.which_attrib = Character_attributes::weight_char,
         .json_key = "weight",
         .value_type = 'i'},
        {.which_attrib = Character_attributes::name_char,
         .json_key = "name",
         .value_type = 's'},
        {.which_attrib = Character_attributes::str_char,
         .json_key = "strength",
         .value_type = 'i'},
        {.which_attrib = Character_attributes::dex_char,
         .json_key = "dexterity",
         .value_type = 'i'},
        {.which_attrib = Character_attributes::con_char,
         .json_key = "constitution",
         .value_type = 'i'},
        {.which_attrib = Character_attributes::itl_char,
         .json_key = "intelligence",
         .value_type = 'i'},
        {.which_attrib = Character_attributes::wis_char,
         .json_key = "wisdom",
         .value_type = 'i'},
        {.which_attrib = Character_attributes::cha_char,
         .json_key = "charisma",
         .value_type = 'i'}};

    return temp;
}

std::vector<Attribute_layout<Character_misc_attributes>>
get_character_miscellaneous_json_layout()
{
    std::vector<Attribute_layout<Character_misc_attributes>> temp {
        {.which_attrib = Character_misc_attributes::alignment_char,
         .json_key = "alignments",
         .value_type = 'v'},
        {.which_attrib = Character_misc_attributes::sexes_char,
         .json_key = "sexes",
         .value_type = 'v'},
        {.which_attrib = Character_misc_attributes::eye_colors_char,
         .json_key = "eye_colors",
         .value_type = 'v'},
        {.which_attrib = Character_misc_attributes::hair_colors_char,
         .json_key = "hair_colors",
         .value_type = 'v'},
        {.which_attrib = Character_misc_attributes::gods_char,
         .json_key = "deities",
         .value_type = 'v'},
        {.which_attrib = Character_misc_attributes::skin_colors_char,
         .json_key = "skin_colors",
         .value_type = 'v'}};
    return temp;
};

std::vector<Attribute_layout<Race_misc_attributes>>
get_race_miscellaneous_json_layout()
{    // race_addition
    std::vector<Attribute_layout<Race_misc_attributes>> temp {
        {.which_attrib = Race_misc_attributes::race_addition,
         .json_key = "race",
         .value_type = 's'},
        {.which_attrib = Race_misc_attributes::min_age_val,
         .json_key = "min_age",
         .value_type = 'i'},
        {.which_attrib = Race_misc_attributes::tier1_age_mod_val,
         .json_key = "tier1_age_mod",
         .value_type = 's'},
        {.which_attrib = Race_misc_attributes::tier2_age_mod_val,
         .json_key = "tier2_age_mod",
         .value_type = 's'},
        {.which_attrib = Race_misc_attributes::tier3_age_mod_val,
         .json_key = "tier3_age_mod",
         .value_type = 's'},
        {.which_attrib = Race_misc_attributes::base_height_val,
         .json_key = "base_height",
         .value_type = 'i'},
        {.which_attrib = Race_misc_attributes::base_weight_val,
         .json_key = "base_weight",
         .value_type = 'i'},
        {.which_attrib = Race_misc_attributes::height_mod_val,
         .json_key = "height_mod",
         .value_type = 's'},
        {.which_attrib = Race_misc_attributes::weight_mod_val,
         .json_key = "weight_mod",
         .value_type = 's'},
        {.which_attrib = Race_misc_attributes::male_height_mod_val,
         .json_key = "male_height_mod",
         .value_type = 'i'},
        {.which_attrib = Race_misc_attributes::male_weight_mod_val,
         .json_key = "male_weight_mod",
         .value_type = 'i'}};
    return temp;
};

std::vector<Attribute_layout<Weapon_attributes>> get_weapon_json_layout()
{    // race_addition
    std::vector<Attribute_layout<Weapon_attributes>> temp {
        {.which_attrib = Weapon_attributes::weapon_name,
         .json_key = "type_of_weapon",
         .value_type = 's'},
        {.which_attrib = Weapon_attributes::weapon_proficiency,
         .json_key = "proficiency",
         .value_type = 's'},
        {.which_attrib = Weapon_attributes::weapon_encumbrance,
         .json_key = "encumbrance",
         .value_type = 's'},
        {.which_attrib = Weapon_attributes::weapon_usefulness,
         .json_key = "usefulness",
         .value_type = 's'},
        {.which_attrib = Weapon_attributes::weapon_cost,
         .json_key = "cost",
         .value_type = 'i'},
        {.which_attrib = Weapon_attributes::weapon_damage_small,
         .json_key = "damage_small",
         .value_type = 's'},
        {.which_attrib = Weapon_attributes::weapon_damage_medium,
         .json_key = "damage_medium",
         .value_type = 's'},
        {.which_attrib = Weapon_attributes::weapon_critical,
         .json_key = "critical",
         .value_type = 's'},
        {.which_attrib = Weapon_attributes::weapon_weight,
         .json_key = "weight",
         .value_type = 'i'},
        {.which_attrib = Weapon_attributes::weapon_type,
         .json_key = "type",
         .value_type = 's'},
        {.which_attrib = Weapon_attributes::weapon_range_increment,
         .json_key = "range_increment",
         .value_type = 'i'}};
    return temp;
};

std::vector<Attribute_layout<Class_features_attributes>> get_json__layout()
{
    std::vector<Attribute_layout<Class_features_attributes>> temp {
        {.which_attrib = Class_features_attributes::feature_flag,
         .json_key = "char_class",
         .value_type = 's'},
        {.which_attrib = Class_features_attributes::feature_name,
         .json_key = "name",
         .value_type = 's'},
        {.which_attrib = Class_features_attributes::feature_bonus,
         .json_key = "bonus",
         .value_type = 'i'},
        {.which_attrib = Class_features_attributes::feature_start_level,
         .json_key = "start_level",
         .value_type = 'i'},
        {.which_attrib = Class_features_attributes::feature_type,
         .json_key = "type",
         .value_type = 's'}

    };

    return temp;
}
}    // namespace my_dnd
