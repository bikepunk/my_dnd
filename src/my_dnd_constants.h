///@file my_dnd_constants.h
///@author Mike H Benton (bikepunk005@hotmail.com)
///@brief Namespace constants
///@version 0.1
///@date 2022-01-28
///
///@copyright Copyright (c) 2022
///
#ifndef MY_DND_MY_DND_CONSTANTS
#define MY_DND_MY_DND_CONSTANTS
#include <QString>

namespace my_dnd {
/// File locations
#ifdef _WIN32
inline const QString pic_file_loc {"C:/Users/bikep/source/repos/my_dnd/images/"};
#else
inline const QString pic_file_loc {"/home/mbenton/projects/my_dnd/images/"};
#endif

/// Level information
constexpr auto start_level {1};
constexpr auto experience_multipler {1000};

/// Integer type attribute flag
constexpr char get_type_int = {'i'};
/// String type attribute flag
constexpr char get_type_string = {'s'};

/// In a vector the position of the strength ability.
constexpr auto str_position {0};
/// In a vector the position of the dexterity ability.
constexpr auto dex_position {1};
/// In a vector the position of the constitution ability.
constexpr auto con_position {2};
/// In a vector the position of the intelligence ability.
/// \note the abbreviation is not the standard int, but itl.
constexpr auto itl_position {3};
/// In a vector the position of the wisdom ability.
constexpr auto wis_position {4};
/// In a vector the position of the charisma ability.
constexpr auto cha_position {5};

//  Misc constants.
/// General not found flag
constexpr auto not_found {-1};

// For brief notes (0 in the array of sentences
constexpr auto brief_location {0};

/// The number of abilities per the manual.
constexpr auto num_abilities {6};

/// The lowest ability that a character can have.
///  Details on Page 8, re-rolling.
constexpr auto low_ability {13};

/// When taken as a whole, the lowest modification total.
///  Details on Page 8, re-rolling.
constexpr auto low_mods {0};

/// For checking mod adjustments, use 5 to subtract from a whole number
/// See \ref mydnd_character::check_modifier.
constexpr auto mod_adjust {5};

/// For bonus spells:
/// (spell_level*multiplier)-offset, use to get mod adjustment.  Then divide
/// by 4.
constexpr auto bonus_spell_multipler {2};
constexpr auto bonus_spell_offset {3};
constexpr auto bonus_spell_numerator {4.0};

/// Skill constants
constexpr auto skills_startmultipler {4};
constexpr auto skills_levelup_bonus {1};

/// The number of rolls allowed when creating a character (drop the lowest)
/// Details on Page 7, Ability scores
constexpr auto ability_rolls {4};

/// Default \ref Attrib_value_type attribute value for integer values.
constexpr auto as_int_default {0};

/// For \ref Attrib_value_type, the value of the string.
const inline QString attrib_default_string {"Empty Attribute"};

/// Minimum intelligence for player characters
/// See Table 2-1
constexpr auto minimum_intelligence {3};

/// \name JSON Keys
/// \anchor JSONKeys
///@{
const inline QString race_key {"race"};
const inline QString class_key {"char_class"};
const inline QString abilities_key {"abilities"};
const inline QString playable_characters_key {"Playable_characters"};
const inline QString character_misc_info_key {"misc"};
const inline QString weapons_key {"Weapons"};
///@}

/// \name Spell Character Info
///@{
const inline QString spell_itl_bonus_class {"Wizard"};
const inline QString spell_wis_bonus_class {"Cleric Druid Paladin Ranger"};
const inline QString spell_cha_bonus_class {"Sorcerer Bard"};
///@}

/// \name Age Tiers
///@{
const inline QString age_tier1 {"Barbarian Rogue Sorcerer"};
const inline QString age_tier2 {"Bard Fighter Paladin Ranger"};
const inline QString age_tier3 {"Cleric Druid Monk Wizard"};
///@}

////////////////////////////////////////////////////////////////////////////////
/// Combat
////////////////////////////////////////////////////////////////////////////////
constexpr auto base_armor_class {10};
}    // namespace my_dnd
#endif
