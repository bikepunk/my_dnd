#ifndef MYDND_CHARACTER_CHARACTER_H
#define MYDND_CHARACTER_CHARACTER_H

#include "class_features.h"
#include "my_dnd.h"
#include "my_dnd_constants.h"

#include <QString>

namespace mydnd_character {

/// \file character.h
/// \brief Provides general classes for a character
/// \page page1_MYDND My DnD character classes
/// \tableofcontents
/// \section Overview_MYDND Overview
/// Provides classes for attributes of characters, spells, monsters, and so on.
/// ## Player Characters (aka: RCC)
/// - There is \a Race, \a Class, and \a Character as the base name (not base class).
///     - Each class relies on attributes
///         - \ref my_dnd::Race_attributes
///         - \ref my_dnd::Class_attributes
///         - \ref my_dnd::Character_attributes
/// - \a information hold attributes of each base name.
///     - For example \ref Race_information has \ref my_dnd::Race_attributes
///     attributes.
/// - \a addition_info and \a info holds information that is not part of playing, but
/// more for character creation.  info is short as a clue that not the information is
/// not part of playing.
/// - \a standard holds a vector of associated base attribute class for non-Character
/// classes.
/// - \a Playable_characters is the equivalent of standard.
/// - There is a a \a misc_info, these are under Character and are not tied to a class
/// or race.
///

////////////////////////////////////////////////////////////////////////////////
/// usings
////////////////////////////////////////////////////////////////////////////////
using my_dnd::Attrib_value_type;

////////////////////////////////////////////////////////////////////////////////
/// forwards
////////////////////////////////////////////////////////////////////////////////

struct Ability_type;
class Race_information;
class Race_additional_information;
class Race_additional_standard;
class Class_information;
class Character_information;
class Character_misc_standard;

bool check_modifier(const Ability_type&);
bool check_ability(const Ability_type&);

int calculate_number_new_skills(const Character_information&);
int calculate_number_new_feats(const Character_information&);
int calculate_next_level(int);
int calculate_bonus_spell(int, int);
int get_bonus_spell(const QString&, const Ability_type&);
QString set_character_qpix(const QString&, const QString&);

my_dnd::attribute_type roll_ability_score();

void calculate_vital_statistics(Race_additional_standard&, Character_information&);

std::vector<QString> get_filtered_god_list(
    const Character_misc_standard& misc_data_in,
    const Character_information& character_data);

////////////////////////////////////////////////////////////////////////////////
/// externs
////////////////////////////////////////////////////////////////////////////////

extern int read_in_json(std::vector<mydnd_character::Race_information>&);
extern int read_in_json(std::vector<Race_additional_information>&);
extern int read_in_json(std::vector<Class_information>&);
extern int read_in_json(std::vector<Character_information>&);
extern int read_in_json(Character_misc_standard&);
extern int write_json(std::vector<Character_information>&);

////////////////////////////////////////////////////////////////////////////////
/// Classes
////////////////////////////////////////////////////////////////////////////////

///
/// Abilities and modifications.
///
struct Ability_type {
    Ability_type();
    void set_modifiers();
    void clear_data();
    int max() const;

    void set_ability_vector()
    {
        abilities.clear();
        abilities.resize(my_dnd::num_abilities);
        abilities[my_dnd::str_position] = str;
        abilities[my_dnd::dex_position] = dex;
        abilities[my_dnd::con_position] = con;
        abilities[my_dnd::itl_position] = itl;
        abilities[my_dnd::wis_position] = wis;
        abilities[my_dnd::cha_position] = cha;
    }
    Attrib_value_type str;
    Attrib_value_type dex;
    Attrib_value_type con;
    Attrib_value_type itl;
    Attrib_value_type wis;
    Attrib_value_type cha;
    Attrib_value_type mod_str;
    Attrib_value_type mod_dex;
    Attrib_value_type mod_con;
    Attrib_value_type mod_itl;
    Attrib_value_type mod_wis;
    Attrib_value_type mod_cha;
    std::vector<Attrib_value_type> abilities;
};

/// TODO: check with class and verbiage
/// Weapon familiarity for classes.
/// If a race/class/special has a weapon familiarity then have weapon and level.
/// \param w is weapon name
/// \param f is familiarity level (e.g., martial)
///
struct Weapon_familiarity {
    Weapon_familiarity(const QString& w, const QString& f):
        weapon {w}, familiarity {f}
    {
    }
    QString weapon;
    QString familiarity;
};

///
/// For bonus fighting certain creature or creature types based on race/class/special.
/// \param c is the creature type or classification
/// \param b is the integer of bonus
///
struct Creature_bonus {
    Creature_bonus(const QString& c, const int b): creature {c}, bonus {b}
    {
    }
    QString creature;
    int bonus {0};
};

///
/// The Information classes contain all attributes specific to that type of race.
/// All race attributes for a race are contained here.  For all player characters:
/// - \ref my_dnd::Race_attributes
/// - \ref get_race_json_layout()
///
class Race_information
{
public:
    explicit Race_information();
    // Gets
    Attrib_value_type get_race_attribute(const my_dnd::Race_attributes&) const;
    Attrib_value_type get_vec_attribute(const my_dnd::Race_attributes&) const;
    QString get_race_name() const    /// Keep simple, but can use the regular get.
    {
        return race.as_string;
    }

    // Sets
    void set_attribute_from_json(const QJsonObject&, const my_dnd::Race_attributes&);
    void set_race_name(const QString& race_name)    /// Set here, then populate.
    {
        race.set_avt(race_name);
    }

private:
    void set_attribute(const QJsonObject&, auto, const my_dnd::Race_attributes&);
    /// \name Race_attributes_variables
    ///@{
    std::vector<Weapon_familiarity> weap_fam;
    std::vector<Creature_bonus> creat_bonus;
    Attrib_value_type race;    ///< Match up with Character_races
    Attrib_value_type favored;    ///< Favored class
    Attrib_value_type addl_lang;    ///< Additional languages
    Attrib_value_type creat_size;
    Attrib_value_type str_adj;
    Attrib_value_type dex_adj;
    Attrib_value_type con_adj;
    Attrib_value_type itl_adj;
    Attrib_value_type wis_adj;
    Attrib_value_type cha_adj;
    Attrib_value_type land_speed;
    Attrib_value_type age;
    Attrib_value_type extra_feat;
    Attrib_value_type no_armor_speed_reduction;
    Attrib_value_type darkvision;
    Attrib_value_type stonecunning;
    Attrib_value_type low_light_vision;
    Attrib_value_type small_size_bonus;
    Attrib_value_type giant_bonus;
    Attrib_value_type stability_bonus;
    Attrib_value_type personality;
    Attrib_value_type personality_brief;
    ///@}
};

///
/// Vector of race information.
/// Call with [] to get a race.
/// Use \ref get_race_list to have a vector of race
/// Use \ref get_race_data for a vector of all \ref Race_information
///
class Race_standard
{
public:
    explicit Race_standard();

    Race_information& operator[](const QString&);

    std::vector<QString> get_race_list() const
    {
        return race_list;
    }

    std::vector<Race_information> get_race_data() const
    {
        return race_data;
    }

private:
    std::vector<Race_information> race_data;    ///< Vector of all race data.
    std::vector<QString> race_list;    ///< Each race for selectors
};

///
/// The Information classes contain all attributes specific to that type of race.
/// All additional race attributes for a race are contained here.
/// Generally, used during character creation.
///
class Race_additional_information
{
public:
    explicit Race_additional_information();
    // Gets
    Attrib_value_type get_race_attribute(const my_dnd::Race_misc_attributes&) const;
    QString get_race_name() const    /// Keep simple, but can use the regular get.
    {
        return race.as_string;
    }

    // Sets
    void set_attribute_from_json(const QJsonObject&,
                                 const my_dnd::Race_misc_attributes&);
    void set_race_name(const QString& race_name)    /// Set here, then populate.
    {
        race.set_avt(race_name);
    }

private:
    void set_attribute(const QJsonObject&, auto, const my_dnd::Race_misc_attributes&);

    Attrib_value_type race;
    Attrib_value_type min_age;
    Attrib_value_type tier1_age_mod;
    Attrib_value_type tier2_age_mod;
    Attrib_value_type tier3_age_mod;
    Attrib_value_type base_height;
    Attrib_value_type base_weight;
    Attrib_value_type height_mod;
    Attrib_value_type weight_mod;
    Attrib_value_type male_height_mod;
    Attrib_value_type male_weight_mod;
};

///
/// Vector of additional race information.
/// Call with [] to get a particular race information.
/// use \ref get_race_data for a vector of all \ref Race_additional_information
///
class Race_additional_standard
{
public:
    explicit Race_additional_standard();

    Race_additional_information& operator[](const QString&);

    std::vector<Race_additional_information> get_race_data()
    {
        return additional_race_data;
    }

private:
    std::vector<Race_additional_information> additional_race_data;
};

///
/// The Information classes contain all attributes specific to that type of class.
/// All class attributes are contained here.  For all player characters:
/// - \ref my_dnd::Race_attributes
/// - \ref get_race_json_layout()
///
class Class_information
{
public:
    explicit Class_information();
    // Gets
    Attrib_value_type get_class_attribute(const my_dnd::Class_attributes&) const;

    QString get_class_type_name() const
    {
        return class_type.as_string;    /// Use to get class.
    }

    std::vector<Feature_value_type> get_features()
    {
        return features;
    }
    // Sets
    void set_attribute_from_json(const QJsonObject&, const my_dnd::Class_attributes&);
    void set_class_type_name(const QString& class_name)
    {
        class_type.set_avt(class_name);    // Use to set class.
    }

private:
    void set_attribute(const QJsonObject&, auto, const my_dnd::Class_attributes&);
    Attrib_value_type class_type;
    Attrib_value_type hit_die;
    Attrib_value_type alingment_restriction;
    Attrib_value_type class_type_description;
    Attrib_value_type class_type_brief;
    Attrib_value_type diety_restrictions;
    Attrib_value_type damage_reduction;
    Attrib_value_type base_skill_points;
    Attrib_value_type bonus_feat_flag {.as_string = "False", .as_int = false};
    std::vector<Attrib_value_type> bonus_feat_levels;
    std::vector<Attrib_value_type> bonus_feat_limits;
    std::vector<Feature_value_type> features;    // flags.
};

///
/// Vector of class information.
/// Call with [] to get a class.
/// Use \ref get_class_list to have a vector of classes.
/// use \ref get_class_data for a vector of all \ref Class_information
///
class Class_standard
{
public:
    explicit Class_standard();

    Class_information& operator[](const QString&);

    std::vector<QString> get_class_list()
    {
        return class_list;
    }

    std::vector<Class_information> get_class_data()
    {
        return class_data;
    }

private:
    std::vector<Class_information> class_data;    ///< Vector of all class data.
    std::vector<QString> class_list;    ///< Each class for selectors
};

///
/// Contains a player characters data.  For race and class, the race and class are
/// listed, but must be matched back to \ref Race_information and \ref
/// Class_information.  The same goes for what spells, and equipment the character
/// will have.  The default constructor is used to populate from a complete
/// character, the new is used when creating a new character; this new populates
/// abilities with random numbers (4d6).  As an information class, the gets and sets
/// are similar.
/// \todo Other stuff like equipment, spells, list of campaigns, deaths:)
/// \todo How to do a saved character--maybe in new
/// \todo A lot member functions that are helpers.  Need to move out.
/// \todo For default constructor--need to check character is valid.  Use
/// get_new_character_flag flag.
///
class Character_information
{
public:
    explicit Character_information();
    explicit Character_information(bool);
    // Gets
    Attrib_value_type get_attribute(const my_dnd::Character_attributes&) const;
    Attrib_value_type get_attribute(const my_dnd::Class_attributes& whats) const
    {
        return class_info.get_class_attribute(whats);
    }
    Attrib_value_type get_attribute(const my_dnd::Race_attributes& whats) const
    {
        return race_info.get_race_attribute(whats);
    }
    QString get_character_name() const    /// Simple, but can use the regular get.
    {
        return character_multi_pass.name.as_string;
    }
    QString get_character_id() const
    {
        return character_multi_pass.id.as_string;
    }
    QString get_race_name() const    /// Keep simple, but can use the regular get.
    {
        return race.as_string;
    }
    QString get_class_type_name() const    /// Keep simple, can use get_
    {
        return class_type.as_string;
    }
    Ability_type get_abilities() const
    {
        return abilities;
    }
    bool get_new_character_flag() const    ///\todo needed?
    {
        return new_character;
    }
    my_dnd::Character_name_type get_multipass() const
    {
        return character_multi_pass;
    }
    my_dnd::Armor_class_data get_armor_class() const
    {
        return armor_class;
    }
    QString get_pic() const
    {
        // QString temp {set_character_qpix(race.as_string, gender.as_string)};
        return qpix.as_string;    // qpix.as_string;
    }

    /// TODO: testing
    Damage_reduction dam_redutc;
    std::vector<QString> class_features;

    // Sets
    void set_attribute_from_json(const QJsonObject&,
                                 const my_dnd::Character_attributes&);
    void set_attribute(const QVariant&, const my_dnd::Character_attributes&);

    void roll_new_character();
    void update_modifiers();
    void set_character_name(const QString& name_in)
    {
        character_multi_pass.name.set_avt(name_in);
    }
    void set_character_id(const QString& id_in)
    {
        character_multi_pass.id.set_avt(id_in);
    }
    void update_new_character_flag(bool new_character_flag)
    {
        new_character = new_character_flag;
    }
    void set_class_data(const Class_information& class_data_in)
    {
        class_info = class_data_in;
    }
    void set_race_data(const Race_information& race_data_in)
    {
        race_info = race_data_in;
    }

private:
    void set_attribute(const QJsonObject&, auto, const my_dnd::Character_attributes&);
    void udpate_race_info();
    void set_armor_class();

    // Race and class for character (saves long calls and not much memory used)
    Race_information race_info;
    Class_information class_info;
    // Player values
    Ability_type abilities;    // Abilities with Race adds
    Ability_type original_abilities;    // Used during character creation
    my_dnd::Character_name_type character_multi_pass {};
    Attrib_value_type level {};
    Attrib_value_type qpix;

    // TODO: Why Set to blanks, not the default.
    Attrib_value_type race = {.as_string = "", .as_int = 0};
    Attrib_value_type class_type = {.as_string = "", .as_int = 0};
    Attrib_value_type deity = {.as_string = "", .as_int = 0};
    Attrib_value_type gender = {.as_string = "", .as_int = 0};
    Attrib_value_type alignment = {.as_string = "", .as_int = 0};
    Attrib_value_type age = {.as_string = "0", .as_int = 0};
    Attrib_value_type height = {.as_string = "0", .as_int = 0};
    Attrib_value_type weight = {.as_string = "0", .as_int = 0};

    // Combat gear and such
    my_dnd::Armor_class_data armor_class;

    // equipped
    Attrib_value_type equipped_shield;
    Attrib_value_type equipped_armor;
    bool new_character {false};    /// Unless explicit, not a new character
};

///
/// Vector of all playable characters.
/// Call with [] to get a character based on multi_pass id.
///
class Player_characters
{
public:
    explicit Player_characters();

    Character_information& operator[](const QString&);

    std::vector<my_dnd::Character_name_type> get_character_name_list()
    {
        return playable_characters_list;
    }

    void read_characters()
    {
        read_in_json(playable_characters);
    }
    void add_character(const Character_information&);
    void write_characters()
    {
        write_json(playable_characters);
    }

private:
    std::vector<Character_information> playable_characters;
    std::vector<my_dnd::Character_name_type> playable_characters_list;
};

///
/// For attributes that are tied to a race or class or to both.  Only used during
/// character creation.
/// \note no information class as not tied to a race or class.
///
class Character_misc_standard
{
public:
    explicit Character_misc_standard()
    {
        read_in_json(*this);
    }

    // gets
    std::vector<Attrib_value_type> get_character_misc_information(
        const my_dnd::Character_misc_attributes& which_to_get) const;
    std::vector<my_dnd::Gods> get_gods() const
    {
        return gods;
    }

    // sets
    void set_vector_attribute(const QJsonObject&,
                              const my_dnd::Character_misc_attributes&);

private:
    std::vector<Attrib_value_type> alignments;
    std::vector<Attrib_value_type> sexes;
    std::vector<Attrib_value_type> eye_colors;
    std::vector<Attrib_value_type> hair_colors;
    std::vector<Attrib_value_type> skin_colors;
    std::vector<my_dnd::Gods> gods;
};

}    // namespace mydnd_character

#endif
