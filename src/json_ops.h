#ifndef MY_DND_JSON_OPS_H
#define MY_DND_JSON_OPS_H

#include "character.h"
#include "die_roll.h"
#include "fighting_gear.h"
#include "json_layouts.h"
#include "my_dnd.h"
#include "my_dnd_constants.h"
#include "my_dnd_enums.h"

#include <QByteArray>
#include <QFile>
#include <QIODevice>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QString>
#include <QStringList>
#include <QVariant>
#include <fstream>
#include <iostream>
#include <string>
#include <typeinfo>
#include <vector>

namespace mydnd_json_common {

QString get_filename(const my_dnd::File_types&);
}    // namespace mydnd_json_common

/// Character names: thanks to https://www.fantasynamegenerators.com/
namespace mydnd_random_name {

const inline QString default_male_name {"Bob"};
const inline QString default_female_name {"Jane"};
const inline QString default_neutral_name {};
const inline QString male_flag {"Male"};

QString generate_name(const QString&, const QString&);
}    // namespace mydnd_random_name

namespace mydnd_combat_gear {
int read_in_json(std::vector<Weapon_information>&);
}

namespace mydnd_character {
/// \file json_ops.h
/// \brief Provides for reading and writing JSON files
/// \page page1_JSON Reading in JSON Mydnd files
/// \tableofcontents
/// \section Overview_JSON Overview
/// The process is simple:
/// - get a file name
/// - open the file (root)
/// - take the root and make a doc
/// - then an object.
/// \subsection FullOverview Full Overview
/// Reading a json file requires:
/// - A vector of \ref my_dnd::Attribute_layout
/// - The vector must be of an enum class of the various items
///     - Keep the names long, but similar to the keys
///     - The enum name should be a short version
///     - Enums are in \ref my_dnd_enums.
/// - Create or add to a class/struct, have a longer name but same base part
///     - The class must have the vector
///     - The class needs to do the job of parsing out all the JSON data
/// - Create the key QString in my_dnd_constants.h. See \ref JSONKeys.
/// - Call the read_in_json with the vector type.
///     - See \ref read_in_json
/// - \ref read_in_json starts the action.  All do similar actions.
///     - Create a QJsonObject based on the key provided or just the \ref
///     my_dnd::File_types
///         - Use \ref mydnd_json_common::prepare_root to get root.  It can filter if
///         desired, set the switch inside.
///     - Process the returned file use member functions of the class that called.
/// \subsection GettingRootOverview Getting the Root Object
/// Create or call a function that will return a QJsonObject of the JSON object.  For
/// example, to read in the class data from character_dict.json, call
/// \ref mydnd_json_common::prepare_root with the \ref
/// my_dnd::File_types::character_attributes.  You will need to filter based on the
/// particular key.
/// - \ref mydnd_json_common::prepare_root actions
///     - Call \ref mydnd_json_common::get_json_root
///         - Pass the required \ref my_dnd::File_types.  It will return a QByteArray
///         of the root.  That is, it returns the whole JSON file.
///         - Calls \ref mydnd_json_common::get_filename for the file name.  Not
///         these are current a hack for the paths.  See \ref HACKS.
///         - Returns a \ref mydnd_json_common::get_json_file.
///     - Create a QJsonDocument from the returned value.
///         - \ref mydnd_json_common::check_json_doc should be used to validate
///     - Create a new object from the document.
///         - \ref mydnd_json_common::check_json_object should be used to validate
///     - Use this object to find an filter the key used. See \ref JSONKeys.
///
/// \page page2_JSON Writing JSON mydnd files
/// \tableofcontents
/// \section Overview_writing Overview or Writing
/// With the object various modes are used to get the key/values.
/// For find see values in my_dnd_constants.h.
/// \author Mike H Benton (bikepunk005@hotmail.com)
/// \copyright Copyright (c) 2022

/// Entry to read in a JSON value.
/// Use my_dnd_enums
int read_in_json(std::vector<Race_information>&);
int read_in_json(std::vector<Race_additional_information>&);
int read_in_json(std::vector<Class_information>&);
int read_in_json(std::vector<Character_information>&);
int read_in_json(Character_misc_standard&);
int read_in_json(std::vector<Features_all>&);

/// Write characters--currently only write.
int write_json(std::vector<Character_information>&);

}    // namespace mydnd_character
#endif
