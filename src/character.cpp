#include "character.h"

#include "json_ops.h"

#include <QDebug>

namespace mydnd_character {

///
/// \brief calculate_number_new_skills
/// \param character_in race, class, level is needed.
/// \return calculated skill points to use for purchase
///
int calculate_number_new_skills(const Character_information& character_in)
{
    auto base_value {
        character_in
            .get_attribute(my_dnd::Class_attributes::base_skills_points_attrib)
            .as_int};
    auto bonus_flag {
        character_in.get_attribute(my_dnd::Race_attributes::extra_feat_attrib)
            .as_int};
    auto itl_mod {character_in.get_abilities().mod_itl.as_int};

    auto skill_points {base_value + itl_mod};

    // New character or level 1?
    if (character_in.get_attribute(my_dnd::Character_attributes::level_char).as_int ==
        my_dnd::start_level) {
        skill_points = (skill_points < 0) ? 0 : skill_points;
        skill_points *= my_dnd::skills_startmultipler;
        if (bonus_flag)
            skill_points += my_dnd::skills_startmultipler;
    }
    else {
        if (bonus_flag)
            skill_points +=
                my_dnd::skills_levelup_bonus;    // bonus is for non-level 1
    }
    return skill_points;
}

///
/// \brief calculate_number_new_feats
/// \param character_in for needed data
/// \return calculated number of feats
///
int calculate_number_new_feats(const Character_information& character_in)
{
    auto bonus_flag {
        character_in.get_attribute(my_dnd::Race_attributes::extra_feat_attrib)
            .as_int};
    auto number_feats {my_dnd::start_level};

    if (bonus_flag) {
        number_feats++;
    }

    auto class_bonus_feat {
        character_in.get_attribute(my_dnd::Class_attributes::bonus_feat_attrib)
            .as_int};
    if (class_bonus_feat) {
        number_feats++;
    }

    return number_feats < 0 ? 0 : number_feats;
}

struct Race_picture {
    QString race_name;
    QString race_pic_base;
};

QString set_character_qpix(const QString& race_in, const QString& sex)
{
    std::vector<Race_picture> race_pictures {
        {"Dwarf", "dwarf"},       {"Human", "human"}, {"Elf", "elf"},
        {"Half_orc", "half_orc"}, {"Gnome", "gnome"}, {"Half_elf", "half_elf"},
        {"Halfling", "halfling"}};
    QString temp;
    for (const auto& i : race_pictures)
        if (i.race_name.contains(race_in))
            temp = i.race_pic_base;

    if (sex == "Female")
        return temp + "_female.png";
    else
        return temp + "_male.png";
}

///
/// \brief get_json_string
/// \param json_in
/// \param key
/// \return the key value in QString format
///
QString get_json_string(const QJsonObject& json_in, auto key)
{
    return json_in[key.first].toString();
}

///
/// \brief get_json_int
/// \param json_in
/// \param key
/// \return The key value in int format
///
int get_json_int(const QJsonObject& json_in, auto key)
{
    return json_in[key.first].toInt();
}

///
/// \brief get_json_bool
/// \param json_in
/// \param key
/// \return the key value in bool format
///
bool get_json_bool(const QJsonObject& json_in, auto key)
{
    return json_in[key.first].toBool();
}

///
/// Calculate next experience level
/// \param current_level is current level
/// \return experience needed to reach next level
///
int calculate_next_level(int current_level)
{
    my_dnd::ability_score_type next_level {0};
    for (auto i = my_dnd::start_level; i <= current_level; ++i) {
        next_level = next_level + (i * my_dnd::experience_multipler);
        // if (i==my_dnd::start_level){next_level=0;}
    }

    return next_level;
}

///
/// Check if modifier scores are above minimum.
/// Used for new? character generation.  Before race selection.
/// If false, allows a re roll.
/// \param abilities_in \ref Ability_type
/// \returns true if scores meet minimum.
/// See \ref my_dnd::low_mods.
///
bool check_modifier(const Ability_type& abilities_in)
{
    auto mod_total {abilities_in.mod_str.as_int};
    mod_total += abilities_in.mod_dex.as_int;
    mod_total += abilities_in.mod_con.as_int;
    mod_total += abilities_in.mod_itl.as_int;
    mod_total += abilities_in.mod_wis.as_int;
    mod_total += abilities_in.mod_cha.as_int;
    qDebug() << mod_total << '\n';
    if (mod_total > my_dnd::low_mods)
        return true;

    return false;
}

///
/// Check if max abilities is not below low_ability and modifier it good.
/// All abilities must have one ability above \ref my_dnd::low_ability.
/// \param abilities_in
/// \return true at least one is above
/// \ref my_dnd::low_ability.
///
bool check_ability(const Ability_type& abilities_in)
{
    auto max_ability_value {abilities_in.max()};

    if (!check_modifier(abilities_in) || (max_ability_value <= my_dnd::low_ability))
        return false;

    return true;
}
///
/// For the 9 spell levels, calculates the number of bonus spells.
/// Table 1-1.
/// \param spell_level
/// \param ability_score
/// \returns number (0 or greater) of bonus spell for the level
///
int calculate_bonus_spell(int spell_level, int ability_score)
{
    auto mod_total {0.0};

    if (!my_dnd::is_even(ability_score))
        --ability_score;

    auto level_adjust {(ability_score + my_dnd::bonus_spell_offset) -
                       (spell_level * my_dnd::bonus_spell_multipler)};

    if (my_dnd::is_even(level_adjust))
        mod_total = (level_adjust / 2 - my_dnd::mod_adjust);
    else
        mod_total = ((level_adjust - 1) / 2 - my_dnd::mod_adjust);

    if (mod_total > 0)
        return std::ceil(mod_total / my_dnd::bonus_spell_numerator);
    return 0;
}

///
///  Determines number of bonus spells for a race.
///  \param class_in The given class
///  \param scores All ability scores
///  \return The bonus 0 on up.  Table 1-1 is reference.
///  \ref calculate_bonus_spell
///
int get_bonus_spell(const QString& class_in, const Ability_type& scores)
{
    constexpr auto min_spell_level {1};
    if (class_in.isEmpty())
        return 0;
    // Determine which ability score to use, then calculate;
    if (my_dnd::spell_itl_bonus_class.contains(class_in))
        return calculate_bonus_spell(min_spell_level, scores.itl.as_int);

    else if (my_dnd::spell_wis_bonus_class.contains(class_in))
        return calculate_bonus_spell(min_spell_level, scores.wis.as_int);

    else if (my_dnd::spell_cha_bonus_class.contains(class_in))
        return calculate_bonus_spell(min_spell_level, scores.cha.as_int);

    // No matter what, 0.
    return 0;
}

///
/// Populates with 4d6 (or what ever set too).
/// \returns \ref my_dnd::die_type with an ability roll
///
my_dnd::attribute_type roll_ability_score()
{
    return my_dnd::roll_drop_lowest(my_dnd::ability_rolls);
}

///
/// For a player character, calculates starting age.
/// \param class_in
/// \param race_data
///
auto calculate_age(const QString& class_in,
                   const Race_additional_information& race_data)
{
    // Age
    // Age is race and class dependent.

    auto roll {0};
    // First from class get tier
    using enum my_dnd::Race_misc_attributes;
    if (my_dnd::age_tier1.contains(class_in)) {
        roll = my_dnd::die_roll(
            race_data.get_race_attribute(tier1_age_mod_val).as_string);
    }
    else if (my_dnd::age_tier2.contains(class_in)) {
        roll = my_dnd::die_roll(
            race_data.get_race_attribute(tier2_age_mod_val).as_string);
    }
    else if (my_dnd::age_tier3.contains(class_in)) {
        roll = my_dnd::die_roll(
            race_data.get_race_attribute(tier3_age_mod_val).as_string);
    }
    auto value = race_data.get_race_attribute(min_age_val).as_int;
    return value + roll;
}

///
/// Age, Height, and Weight based on Tables 6-4 and 6-5.
/// Unique to new character generation.
/// \param races_data_in gotta get to the data
/// \param character_data to extract current data and return
///
void calculate_vital_statistics(Race_additional_standard& races_data_in,
                                Character_information& character_data)
{
    // Get race and sex ready to set up male mods if needed.

    // No Race (so no need to do anything).
    auto race {character_data.get_race_name()};
    if (race.isEmpty()) {
        return;
    }
    auto race_data = races_data_in[race];

    // Sex
    auto sex_in {
        character_data.get_attribute(my_dnd::Character_attributes::gender_char)
            .as_string};

    // Female is always a base, male add or subtract as needed
    auto weight_sex_mod {0};
    auto height_sex_mod {0};

    // Set the mods if male.
    if (QString::localeAwareCompare(sex_in, "Male") == 0)    // True for QT
    {
        weight_sex_mod =
            race_data
                .get_race_attribute(my_dnd::Race_misc_attributes::male_weight_mod_val)
                .as_int;
        height_sex_mod =
            race_data
                .get_race_attribute(my_dnd::Race_misc_attributes::male_height_mod_val)
                .as_int;
    }

    // Will be using a lot so might as will bring in.
    using enum my_dnd::Character_attributes;

    // Weight
    auto value {
        race_data.get_race_attribute(my_dnd::Race_misc_attributes::base_weight_val)
            .as_int};
    auto roll {my_dnd::die_roll(
        race_data.get_race_attribute(my_dnd::Race_misc_attributes::weight_mod_val)
            .as_string)};
    character_data.set_attribute((value + roll + weight_sex_mod), weight_char);

    // Height
    value =
        race_data.get_race_attribute(my_dnd::Race_misc_attributes::base_height_val)
            .as_int;
    roll = my_dnd::die_roll(
        race_data.get_race_attribute(my_dnd::Race_misc_attributes::height_mod_val)
            .as_string);

    character_data.set_attribute((value + roll + height_sex_mod), height_char);

    // Age
    // Age is race and class dependent.
    auto class_in {character_data.get_class_type_name()};
    character_data.set_attribute(calculate_age(class_in, race_data), age_char);
}

///
///  First pass--simple for Barbarian and Lawful.
///
std::vector<QString> get_filtered_god_list(
    const Character_misc_standard& misc_data_in,
    const Character_information& character_data)
{
    auto get_gods {misc_data_in.get_gods()};
    // auto race_filter {character_data.get_race_name()};
    auto class_filter {character_data.get_class_type_name()};
    // auto alignment_filter {
    //    character_data.get_character_attribute(Character_attributes::alignment_char)};

    std::vector<QString> temp {};
    for (const auto& i : get_gods) {
        QString temp_name;

        // if (i.alignment.isEmpty())
        temp_name = i.god_name;

        if (i.alignment.contains("Lawful") && class_filter == "Barbarian")
            temp_name = "";

        if (!temp_name.isEmpty()) {
            temp.push_back(temp_name);
        }
    }
    return temp;
}

////////////////////////////////////////////////////////////////////////////////
/// Class member functions
////////////////////////////////////////////////////////////////////////////////

// Pretty place holder
Ability_type::Ability_type() {};

///
/// Set modifiers based on ability
/// \todo check!
///
void Ability_type::set_modifiers()
{
    mod_str.set_avt(my_dnd::calculate_ability_modifier(str).as_int);
    mod_dex.set_avt(my_dnd::calculate_ability_modifier(dex).as_int);
    mod_con.set_avt(my_dnd::calculate_ability_modifier(con).as_int);
    mod_itl.set_avt(my_dnd::calculate_ability_modifier(itl).as_int);
    mod_wis.set_avt(my_dnd::calculate_ability_modifier(wis).as_int);
    mod_cha.set_avt(my_dnd::calculate_ability_modifier(cha).as_int);
}

///
/// Just set values not \ref my_dnd::attrib_default_string
///
void Ability_type::clear_data()
{
    using my_dnd::as_int_default;
    str.set_avt(as_int_default);
    dex.set_avt(as_int_default);
    con.set_avt(as_int_default);
    itl.set_avt(as_int_default);
    wis.set_avt(as_int_default);
    cha.set_avt(as_int_default);
    mod_str.set_avt(as_int_default);
    mod_dex.set_avt(as_int_default);
    mod_con.set_avt(as_int_default);
    mod_itl.set_avt(as_int_default);
    mod_wis.set_avt(as_int_default);
    mod_cha.set_avt(as_int_default);
    set_ability_vector();
}

///
/// Get the max for all abilities.
/// \note So far, used only in character creation.
///
int Ability_type::max() const
{
    auto max_value {(str.as_int > dex.as_int) ? str : dex};
    max_value = (max_value.as_int > con.as_int) ? max_value : con;
    max_value = (max_value.as_int > itl.as_int) ? max_value : itl;
    max_value = (max_value.as_int > wis.as_int) ? max_value : wis;
    max_value = (max_value.as_int > cha.as_int) ? max_value : cha;

    return max_value.as_int;
}

// Pretty place holder
Race_information::Race_information() {};

///
/// Generic get for any race attribute.
/// \ref race is possible, bus use \ref get_race_name
/// \param which_to_get
/// \return The attribute requested--only one.
///
Attrib_value_type Race_information::get_race_attribute(
    const my_dnd::Race_attributes& which_to_get) const
{
    switch (which_to_get) {
        using enum my_dnd::Race_attributes;
    case race_attrib:
        return race;
    case favored_attrib:
        return favored;
    case addl_lang_attrib:
        return addl_lang;
    case creat_size_attrib:
        return creat_size;
    case str_adj_attrib:
        return str_adj;
    case dex_adj_attrib:
        return dex_adj;
    case con_adj_attrib:
        return con_adj;
    case itl_adj_attrib:
        return itl_adj;
    case wis_adj_attrib:
        return wis_adj;
    case cha_adj_attrib:
        return cha_adj;
    case land_speed_attrib:
        return land_speed;
    case extra_feat_attrib:
        return extra_feat;
    case no_armor_speed_reduction_attrib:
        return no_armor_speed_reduction;
    case darkvision_attrib:
        return darkvision;
    case stonecunning_attrib:
        return stonecunning;
    case low_light_vision_attrib:
        return low_light_vision;
    case small_size_bonus_attrib:
        return small_size_bonus;
    case giant_bonus_attrib:
        return giant_bonus;
    case stability_bonus_attrib:
        return stability_bonus;
    case personality_attrib:
        return personality;
    case personality_brief_attrib:
        return personality_brief;
        // Will need separate later.
    case weap_fam_attrib:
        // weap_fam;
    case creat_bonus_attrib:
        return this->get_vec_attribute(which_to_get);    // creat_bonus;
    }
    return Attrib_value_type {};
}

///
/// Generic get for vector attributes.
/// \param attrib_in
/// \return A value associated in the vector
/// \todo Not implemented or tested.
///
Attrib_value_type Race_information::get_vec_attribute(
    const my_dnd::Race_attributes& attrib_in) const
{
    QString temp;
    if (attrib_in == my_dnd::Race_attributes::weap_fam_attrib)
        for (const auto& i : weap_fam)
            temp.append(i.weapon + " " + i.familiarity + '\n');
    else if (attrib_in == my_dnd::Race_attributes::creat_bonus_attrib)
        for (const auto& i : creat_bonus)
            temp.append(i.creature + " " + QString::number(i.bonus) + '\n');
    return {temp};
}

///
/// Populate the various attributes with the JSON data
/// This is called as the setter; however, use \ref set_race_name is to set
/// race. \param json_in A json object for each race \param which_to_set the
/// enum of the attribute \note that the briefs are set using the full JSON
/// Array, and treat Array like a vector.
///
void Race_information::set_attribute_from_json(
    const QJsonObject& json_in, const my_dnd::Race_attributes& which_to_set)
{
    // Static here as will only be used during character creation.
    static auto json_layout {my_dnd::get_race_json_layout()};
    auto key {get_key_data<my_dnd::Race_attributes>(json_layout, which_to_set)};
    Race_information::set_attribute(json_in, key, which_to_set);
}

void Race_information::set_attribute(const QJsonObject& json_in, auto key,
                                     const my_dnd::Race_attributes& which_to_set)
{
    switch (which_to_set) {
        using enum my_dnd::Race_attributes;
    case race_attrib:
        if (race.as_string.isEmpty()) {
            race.set_avt(get_json_string(json_in, key));
        }
        break;
    case favored_attrib:
        favored.set_avt(get_json_string(json_in, key));
        break;
    case addl_lang_attrib:
        addl_lang.set_avt(get_json_string(json_in, key));
        break;
    case creat_size_attrib:
        creat_size.set_avt(get_json_string(json_in, key));
        break;
    case personality_brief_attrib:
        personality_brief.set_avt(get_json_string(json_in, key));
        break;
    case str_adj_attrib:
        str_adj.set_avt(get_json_int(json_in, key));
        break;
    case dex_adj_attrib:
        dex_adj.set_avt(get_json_int(json_in, key));
        break;
    case con_adj_attrib:
        con_adj.set_avt(get_json_int(json_in, key));
        break;
    case itl_adj_attrib:
        itl_adj.set_avt(get_json_int(json_in, key));
        break;
    case wis_adj_attrib:
        wis_adj.set_avt(get_json_int(json_in, key));
        break;
    case cha_adj_attrib:
        cha_adj.set_avt(get_json_int(json_in, key));
        break;
    case land_speed_attrib:
        land_speed.set_avt(get_json_int(json_in, key));
        break;
    case extra_feat_attrib:
        extra_feat.set_avt(get_json_bool(json_in, key));
        break;
    case no_armor_speed_reduction_attrib:
        no_armor_speed_reduction.set_avt(get_json_bool(json_in, key));
        break;
    case darkvision_attrib:
        darkvision.set_avt(get_json_bool(json_in, key));
        break;
    case stonecunning_attrib:
        stonecunning.set_avt(get_json_bool(json_in, key));
        break;
    case small_size_bonus_attrib:
        small_size_bonus.set_avt(get_json_bool(json_in, key));
    case low_light_vision_attrib:
        low_light_vision.set_avt(get_json_bool(json_in, key));
    case giant_bonus_attrib:
        giant_bonus.set_avt(get_json_bool(json_in, key));
    case stability_bonus_attrib:
        stability_bonus.set_avt(get_json_bool(json_in, key));
    case weap_fam_attrib: {
        QJsonObject weapon_mods {json_in[key.first].toObject()};
        for (const auto& i : weapon_mods.keys()) {
            weap_fam.push_back(Weapon_familiarity(i, weapon_mods[i].toString()));
        }
    } break;
    case creat_bonus_attrib: {
        QJsonObject creature_mods {json_in[key.first].toObject()};
        for (const auto& i : creature_mods.keys()) {
            creat_bonus.push_back(Creature_bonus(i, creature_mods[i].toInt()));
        }
    } break;
    // Sets brief and full
    case personality_attrib: {
        QJsonArray personality_info {json_in[key.first].toArray()};
        personality_brief = {personality_info[my_dnd::brief_location].toString(),
                             my_dnd::as_int_default};

        QString temp;
        for (const auto& i : qAsConst(personality_info)) {
            temp.append(i.toString());
            temp.append(" ");
        }
        personality = {temp, my_dnd::as_int_default};
    } break;
        // default:
        //    qDebug() << "Not a key: " << key;
    }
}

///
/// Contains all the information on race.
/// Used as reference when creating a character.
/// For in play, the \ref Player_characters an \ref Character_information class
/// is used. Call is simply using [race] to get information about a race. \ref
/// race_list is a list of races for use in selectors. \ref race_data is a
/// vector of \ref Race_information for each race.
///
Race_standard::Race_standard()
{
    // Only do once, clear out old data before generating again.
    race_data.clear();
    race_list.clear();

    // First step is populating the race data from JSON.

    /// \note \ref read_in_json calls \ref Race_information to complete \ref
    /// race_data.
    read_in_json(race_data);
    // extern template int read_in_json<mydnd_character::Race_information>(
    // std::vector<mydnd_character::Race_information>);
    // Put the race_data into the list.
    std::transform(race_data.begin(), race_data.end(), std::back_inserter(race_list),
                   [](const Race_information& i) {
                       return i
                           .get_race_attribute(my_dnd::Race_attributes::race_attrib)
                           .as_string;
                   });
}

///
/// Race_standard::operator []
/// \param race_in
/// \return the \ref Race_information of the race_in.
///
Race_information& Race_standard::operator[](const QString& race_in)
{
    // Find the attribute passed an attribute enum class
    auto t = std::find_if(race_data.begin(), race_data.end(),
                          [&cm = race_in](const Race_information& m) -> bool {
                              if (QString::compare(cm, m.get_race_name()) == 0)
                                  return true;
                              return false;
                          });
    if (t == std::end(race_data))
        mydnd_err::error("Not a valid race in Race_standard::race[]");

    return *t;
}

// Pretty place holder.
Race_additional_information::Race_additional_information() {};

///
/// Returns additional race attribute.
/// For \ref race, use \ref get_race_name
/// \param which_to_get
/// \return \ref my_dnd::Attrib_value_type
/// \note \ref my_dnd::attrib_default_string for value if not set.
///
Attrib_value_type Race_additional_information::get_race_attribute(
    const my_dnd::Race_misc_attributes& which_to_get) const
{
    switch (which_to_get) {
        using enum my_dnd::Race_misc_attributes;
    case race_addition:
        return race;
    case min_age_val:
        return min_age;
    case tier1_age_mod_val:
        return tier1_age_mod;
    case tier2_age_mod_val:
        return tier2_age_mod;
    case tier3_age_mod_val:
        return tier3_age_mod;
    case base_height_val:
        return base_height;
    case base_weight_val:
        return base_weight;
    case height_mod_val:
        return height_mod;
    case weight_mod_val:
        return weight_mod;
    case male_height_mod_val:
        return male_height_mod;
    case male_weight_mod_val:
        return male_weight_mod;
        // creat_bonus;
    }
    return Attrib_value_type {};
}

///
/// Populate the various attributes with the JSON data
/// This is called as the setter; however, use \ref set_race_name is to set
/// race. \param json_in A json object for each race \param which_to_set the
/// enum of the attribute \note that the briefs are set using the full JSON
/// Array, and treat Array like a vector.
///
void Race_additional_information::set_attribute_from_json(
    const QJsonObject& json_in, const my_dnd::Race_misc_attributes& which_to_set)
{
    // Static here as will only be used during character creation.
    static auto json_layout {my_dnd::get_race_miscellaneous_json_layout()};
    auto key {get_key_data<my_dnd::Race_misc_attributes>(json_layout, which_to_set)};
    set_attribute(json_in, key, which_to_set);
}

///
/// Generic set for JSON QString
/// \param value_in
/// \param which_to_set
/// \note \ref race can be set here, but use \ref set_race_name
///
void Race_additional_information::set_attribute(
    const QJsonObject& json_in, auto key,
    const my_dnd::Race_misc_attributes& which_to_set)
{
    switch (which_to_set) {
        using enum my_dnd::Race_misc_attributes;
    case race_addition:
        if (race.as_string.isEmpty()) {
            race.set_avt(get_json_string(json_in, key));
        }
        break;
    case tier1_age_mod_val:
        tier1_age_mod.set_avt(get_json_string(json_in, key));
        break;
    case tier2_age_mod_val:
        tier2_age_mod.set_avt(get_json_string(json_in, key));
        break;
    case tier3_age_mod_val:
        tier3_age_mod.set_avt(get_json_string(json_in, key));
        break;
    case height_mod_val:
        height_mod.set_avt(get_json_string(json_in, key));
        break;
    case weight_mod_val:
        weight_mod.set_avt(get_json_string(json_in, key));
        break;
    case min_age_val:
        min_age.set_avt(get_json_int(json_in, key));
        break;
    case base_height_val:
        base_height.set_avt(get_json_int(json_in, key));
        break;
    case base_weight_val:
        base_weight.set_avt(get_json_int(json_in, key));
        break;
    case male_height_mod_val:
        male_height_mod.set_avt(get_json_int(json_in, key));
        break;
    case male_weight_mod_val:
        male_weight_mod.set_avt(get_json_int(json_in, key));
        break;
    default:
        break;
    }
}

///
/// Contains all the additional information on race.
/// Used as reference when creating a character.
/// Call is simply using [race] to get additional information about a race.
/// \ref additional_race_data is a vector of \ref Race_additional_information
/// for each race.
///
Race_additional_standard::Race_additional_standard()
{
    // Only do once, clear out old data before generating again.
    additional_race_data.clear();

    // First step is populating the race data from JSON.

    /// \note \ref read_in_json calls \ref Race_information to complete
    /// race data.
    read_in_json(additional_race_data);
}

///
/// Race_additional_standard::operator []
/// \param race_in
/// \return the \ref Race_information of the race_in.
///
Race_additional_information& Race_additional_standard::operator[](
    const QString& race_in)
{
    // Find the attribute passed an attribute enum class
    auto t =
        std::find_if(additional_race_data.begin(), additional_race_data.end(),
                     [&cm = race_in](const Race_additional_information& m) -> bool {
                         if (QString::compare(cm, m.get_race_name()) == 0)
                             return true;
                         return false;
                     });

    if (t == std::end(additional_race_data))
        mydnd_err::error("Not a valid race in Race_additional_standard::race[]");

    return *t;
}

// Pretty place holder
Class_information::Class_information() {};

///
/// Returns class attribute.
/// For \ref class_type, use \ref get_class_type_name
/// \param which_to_get
/// \return \ref my_dnd::Attrib_value_type
/// \note \ref my_dnd::attrib_default_string for value if not set.
///
Attrib_value_type Class_information::get_class_attribute(
    const my_dnd::Class_attributes& which_to_get) const
{
    switch (which_to_get) {
        using enum my_dnd::Class_attributes;
    case class_attrib:
        return class_type;
    case hit_die_attrib:
        return hit_die;
    case align_restriction_attrib:
        return alingment_restriction;
    case class_brief_attrib:
        return class_type_brief;
    case class_description_attrib:
        return class_type_description;
    case deity_restriction_attrib:
        return diety_restrictions;
    case damage_reduction_attrib:
        return damage_reduction;
    case base_skills_points_attrib:
        return base_skill_points;
    case bonus_feat_attrib:
        return bonus_feat_flag;
    case bonus_feat_level_up_pattern_attrib:    // Dummy
        return Attrib_value_type {};
        break;
    case bonus_feat_limit_attrib:    // Dummy
        return Attrib_value_type {};
        break;
    }
    return Attrib_value_type {};
}

///
/// Populate the various attributes with the JSON data
/// This is called as the setter; however, \ref set_class_type_name is used to
/// set the class. \param json_in A json object for each race \param
/// which_to_set the enum of the attribute \ref my_dnd::Class_attributes. \note
/// that the briefs are set using the full JSON Array, and treat the JSONArray
/// like a vector.
///
void Class_information::set_attribute_from_json(
    const QJsonObject& json_in, const my_dnd::Class_attributes& which_to_set)
{
    // Static here as will only be used during character creation.
    static auto json_layout {my_dnd::get_class_json_layout()};
    auto key {get_key_data<my_dnd::Class_attributes>(json_layout, which_to_set)};
    Class_information::set_attribute(json_in, key, which_to_set);
}

///
/// Generic set for JSON QString
/// \param value_in
/// \param which_to_set
/// \note \ref class_type can be set here, but use \ref set_class_type_name
///
void Class_information::set_attribute(const QJsonObject& json_in, auto key,
                                      const my_dnd::Class_attributes& which_to_set)
{

    switch (which_to_set) {
        using enum my_dnd::Class_attributes;
    case class_attrib:
        if (class_type.as_string.isEmpty()) {
            class_type.set_avt(get_json_string(json_in, key));
        }
        break;
    case align_restriction_attrib:
        alingment_restriction.set_avt(get_json_string(json_in, key));
        break;
    case hit_die_attrib:
        hit_die.set_avt(get_json_string(json_in, key));
        break;
    case deity_restriction_attrib:
        diety_restrictions.set_vt(get_json_bool(json_in, key));
        break;
    case class_brief_attrib:
        break;
    case damage_reduction_attrib:
        damage_reduction.set_avt(get_json_string(json_in, key));
        break;
    case base_skills_points_attrib:
        base_skill_points.set_avt(get_json_int(json_in, key));
        break;
    case bonus_feat_attrib:
        bonus_feat_flag.set_avt(get_json_bool(json_in, key));
        break;
    case bonus_feat_level_up_pattern_attrib: {
        QJsonArray level_pattern {json_in[key.first].toArray()};
        for (const auto& i : level_pattern) {
            bonus_feat_levels.push_back(
                {.as_string = i.toString(), .as_int = i.toInt()});
        }
    } break;
    case bonus_feat_limit_attrib:    /// TODO: vector bonus_feat_limits
    {
        QJsonArray feat_limits {json_in[key.first].toArray()};
        for (const auto& i : feat_limits) {
            bonus_feat_limits.push_back({.as_string = i.toString()});
        }
    } break;
    case class_description_attrib: {
        QJsonArray class_type_info {json_in[key.first].toArray()};
        class_type_brief = {class_type_info[my_dnd::brief_location].toString(),
                            my_dnd::as_int_default};
        /// \todo Move to helper as common in a few places.
        QString temp;
        for (const auto& i : qAsConst(class_type_info)) {
            temp.append(i.toString());
            temp.append(" ");
        }
        class_type_description = {temp, my_dnd::as_int_default};
    } break;
    }
}

///
/// Create information about all classes
/// Used during character creation as player characters need this information
/// \ref Class_information
///
Class_standard::Class_standard()
{
    read_in_json(class_data);

    // Fill in class names based on class_data;
    class_list.clear();
    std::transform(
        class_data.begin(), class_data.end(), std::back_inserter(class_list),
        [](const Class_information& i) { return i.get_class_type_name(); });
}

///
/// Contains all the information on class.
/// Used as reference when creating a character.
/// For in play, the \ref Player_characters an \ref Character_information class
/// is used. Call is simply using [class_name] to get information about a class.
/// \ref class_list is a list of races for use in selectors.
/// \ref class_data is a vector of \ref Class_information for each class.
/// \param class_in
///
Class_information& Class_standard::operator[](const QString& class_in)
{
    // Find the attribute passed an attribute enum class
    auto t = std::find_if(class_data.begin(), class_data.end(),
                          [&cm = class_in](const Class_information& m) -> bool {
                              if (QString::compare(cm, m.get_class_type_name()) == 0)
                                  return true;
                              return false;
                          });
    if (t == std::end(class_data))
        mydnd_err::error("Not a valid class in class[]");

    return *t;
}

/// TODO: implement
Character_information::Character_information()
{
    // ability modifiers.
    // r/c/e stuff.
}

///
/// Generate a new character--roll for abilities.
/// if not set to true, will bomb
/// \param generate set to true
/// \todo Not fully implemented
///
Character_information::Character_information(bool generate): new_character(generate)
{
    if (new_character == false)
        mydnd_err::error("False passed to Character when generating new character");

    roll_new_character();
    level.set_avt(my_dnd::start_level);

    character_multi_pass.id.set_avt(my_dnd::character_id_generator());
}

///
/// Generic get for any character attribute.
/// \ref character_multi_pass, \ref race, and \ref class_type are best though
/// direct gets. \param which_to_get \return The attribute requested--only one.
///
Attrib_value_type Character_information::get_attribute(
    const my_dnd::Character_attributes& which_to_get) const
{
    switch (which_to_get) {
        using enum my_dnd::Character_attributes;
        // case Character_attributes::name_char:
        //   return name;
    case race_char:
        return race;
    case class_type_char:
        return class_type;
    case deity_char:
        return deity;
    case gender_char:
        return gender;
    case alignment_char:
        return alignment;
    case level_char:
        return level;
    case age_char:
        return age;
    case height_char:
        return height;
    case weight_char:
        return weight;
    case name_char:
        return character_multi_pass.name;
    case id_char:
        return character_multi_pass.id;
    case str_char:
        return abilities.str;
    case dex_char:
        return abilities.dex;
    case con_char:
        return abilities.con;
    case itl_char:
        return abilities.itl;
    case wis_char:
        return abilities.wis;
    case cha_char:
        return abilities.cha;
    }
    return Attrib_value_type {};
}

///
/// Populate the various attributes with the JSON data
/// This is called as the setter; however, \ref set_character_name is used to
/// set the characters name. \param json_in A json object for each race \param
/// which_to_set the enum of the attribute \note that the briefs are set using
/// the full JSON Array, and treat Array like a vector.
///
void Character_information::set_attribute_from_json(
    const QJsonObject& json_in, const my_dnd::Character_attributes& which_to_set)
{
    // Static here as will only be used during character creation.
    static auto json_layout {my_dnd::get_player_character_json_layout()};
    auto key {get_key_data<my_dnd::Character_attributes>(json_layout, which_to_set)};

    Character_information::set_attribute(json_in, key, which_to_set);
}

void Character_information::set_attribute(
    const QJsonObject& json_in, auto key,
    const my_dnd::Character_attributes& which_to_set)
{
    switch (which_to_set) {
        using enum my_dnd::Character_attributes;
    case name_char:
        set_attribute(get_json_string(json_in, key), name_char);
        break;
    case race_char:
        set_attribute(get_json_string(json_in, key), race_char);
        break;
    case class_type_char:
        set_attribute(get_json_string(json_in, key), class_type_char);
        break;
    case deity_char:
        set_attribute(get_json_string(json_in, key), deity_char);
        break;
    case gender_char:
        set_attribute(get_json_string(json_in, key), gender_char);
        break;
    case alignment_char:
        set_attribute(get_json_string(json_in, key), alignment_char);
        break;
    case id_char:
        set_attribute(get_json_string(json_in, key), id_char);
        break;
    case level_char:
        set_attribute(get_json_string(json_in, key), level_char);
        break;
    case age_char:
        set_attribute(get_json_string(json_in, key), age_char);
        break;
    case height_char:
        set_attribute(get_json_string(json_in, key), height_char);
        break;
    case weight_char:
        set_attribute(get_json_string(json_in, key), weight_char);
        break;
    case str_char:
        set_attribute(get_json_string(json_in, key), str_char);
        break;
    case dex_char:
        set_attribute(get_json_string(json_in, key), dex_char);
        break;
    case con_char:
        set_attribute(get_json_string(json_in, key), con_char);
        break;
    case itl_char:
        set_attribute(get_json_string(json_in, key), itl_char);
        break;
    case wis_char:
        set_attribute(get_json_string(json_in, key), wis_char);
        break;
    case cha_char:
        set_attribute(get_json_string(json_in, key), cha_char);
        break;
    }
}

void Character_information::set_attribute(
    const QVariant& value_in, const my_dnd::Character_attributes& which_to_set)
{
    switch (which_to_set) {
        using enum my_dnd::Character_attributes;
    case name_char:
        character_multi_pass.name.set_avt(value_in.toString());
        break;
    case race_char:
        race.set_avt(value_in.toString());
        qpix.set_avt(set_character_qpix(race.as_string, gender.as_string));
        // if (!abilities.empty())
        // update_abilities_for_race();
        break;
    case class_type_char:
        class_type.set_avt(value_in.toString());
        break;
    case deity_char:
        deity.set_avt(value_in.toString());
        break;
    case gender_char:
        gender.set_avt(value_in.toString());
        qpix.set_avt(set_character_qpix(race.as_string, gender.as_string));
        break;
    case alignment_char:
        alignment.set_avt(value_in.toString());
        break;
    case id_char:
        character_multi_pass.id.set_avt(value_in.toString());
        break;
    case level_char:
        level.set_avt(value_in.toInt());
        break;
    case age_char:
        age.set_avt(value_in.toInt());
        break;
    case height_char:
        height.set_avt(value_in.toInt());
        break;
    case weight_char:
        weight.set_avt(value_in.toInt());
        break;
    case str_char:
        abilities.str.set_avt(value_in.toInt());
        break;
    case dex_char:
        abilities.dex.set_avt(value_in.toInt());
        break;
    case con_char:
        abilities.con.set_avt(value_in.toInt());
        break;
    case itl_char:
        abilities.itl.set_avt(value_in.toInt());
        break;
    case wis_char:
        abilities.wis.set_avt(value_in.toInt());
        break;
    case cha_char:
        abilities.cha.set_avt(value_in.toInt());
        break;
    default:
        break;
    }
}

///
/// Provide ability rolls for a new character.
/// \ref abilities
/// \ref Ability_type
/// /note Sets sorted.
///
void Character_information::roll_new_character()
{
    std::vector<my_dnd::attribute_type> temp;
    for (auto i = 0; i != my_dnd::num_abilities; ++i)
        temp.push_back(roll_ability_score());
    std::ranges::sort(temp);

    abilities.clear_data();    // Clear out if exist
    abilities.str.set_avt(temp[my_dnd::str_position]);
    abilities.dex.set_avt(temp[my_dnd::dex_position]);
    abilities.con.set_avt(temp[my_dnd::con_position]);
    abilities.itl.set_avt(temp[my_dnd::itl_position]);
    abilities.wis.set_avt(temp[my_dnd::wis_position]);
    abilities.cha.set_avt(temp[my_dnd::cha_position]);

    original_abilities.clear_data();
    original_abilities = abilities;

    udpate_race_info();    // If a race is already selected.
    abilities.set_ability_vector();
    original_abilities.set_ability_vector();
    abilities.set_modifiers();
}

///
/// Sets modifiers and associated attributes as needed.
/// \todo Add more as work is done.
///
void Character_information::update_modifiers()
{
    if (new_character)    // With a new char, need to update based on race
        udpate_race_info();
    abilities.set_modifiers();

    set_armor_class();
}

///
/// Updates the race.
/// When creating a character, \ref race can be null, so a quick check is done.
///
void Character_information::udpate_race_info()
{
    if (race.as_string.isEmpty())
        return;    // Nothing to see here.

    // Get the race info for the selected race
    // My use a few times, keep between selections.
    static Race_standard race_temp;
    race_info = race_temp[race.as_string];
}

///
/// Sets all aspects of armor class.
/// \todo Add rest once implemented.
///
void Character_information::set_armor_class()
{
    auto total_ac {my_dnd::base_armor_class};    // start with base.
    int size_bonus {
        race_info.get_race_attribute(my_dnd::Race_attributes::small_size_bonus_attrib)
            .as_int};
    armor_class.size_modifer.set_avt((size_bonus == true ? 1 : 0));
    armor_class.dexterity_modifer.set_avt(abilities.mod_dex.as_int);
    total_ac +=
        armor_class.dexterity_modifer.as_int + armor_class.size_modifer.as_int;
    armor_class.total_ac.set_avt(total_ac);
}
///
/// Loads and writes playable character data.
/// Data is in a vector of type Character.
/// \warning Make sure any new character data is saved, as this clears
/// everything.
///
Player_characters::Player_characters()
{
    // Told you so.
    playable_characters.clear();
    playable_characters_list.clear();
    read_characters();

    // First step is populating the race data from JSON.
    /// \note \ref read_in_json calls \ref Character_information to complete \ref
    /// playable_characters.
    read_in_json(playable_characters);

    // Put the race_data into the list.
    std::transform(playable_characters.begin(), playable_characters.end(),
                   std::back_inserter(playable_characters_list),
                   [](Character_information i) { return i.get_multipass(); });
    std::sort(playable_characters_list.begin(), playable_characters_list.end());
}

///
/// Character_information::operator []
/// \param character_id_in
/// \return the \ref Character_information of the character_id.
///
Character_information& Player_characters::operator[](const QString& character_id_in)
{
    // Find the attribute passed an attribute enum class
    auto t =
        std::find_if(playable_characters.begin(), playable_characters.end(),
                     [&cm = character_id_in](const Character_information& m) -> bool {
                         if (QString::compare(cm, m.get_character_id()) == 0)
                             return true;
                         return false;
                     });
    if (t == std::end(playable_characters))
        mydnd_err::error("Not a valid race in Playable_characters::operator[]");

    return *t;
}

///
/// Add a character to the JSON file
/// \param new_char
/// \todo Check for valid!
///
void Player_characters::add_character(const Character_information& new_char)
{
    playable_characters.push_back(new_char);
}

///
/// Generic get for any character misc attribute.
/// \param which_to_get
/// \return The attribute requested--only one.
/// \note Returns a vector.
/// \note For /ref gods use /ref get_gods.
///
std::vector<Attrib_value_type> Character_misc_standard::
    get_character_misc_information(
        const my_dnd::Character_misc_attributes& which_to_get) const
{
    // TODO update
    std::vector<Attrib_value_type> temp {};
    switch (which_to_get) {
        using enum my_dnd::Character_misc_attributes;
    case alignment_char:
        return alignments;
    case sexes_char:
        return sexes;
    case eye_colors_char:
        return eye_colors;
    case hair_colors_char:
        return hair_colors;
    case skin_colors_char:
        return skin_colors;
    case gods_char:    // TODO implement
        return temp;
    case gold_char:    // TODO implement
        return temp;
    };

    return temp;
}

///
/// Generic set for misc information
/// \param json_in
/// \param which_to_set
///
void Character_misc_standard::set_vector_attribute(
    const QJsonObject& json_in, const my_dnd::Character_misc_attributes& which_to_set)
{

    Attrib_value_type attrib_temp;    // most will use
    switch (which_to_set) {
        using enum my_dnd::Character_misc_attributes;
    case gold_char:
        break;    // TODO
    case alignment_char: {
        auto temp {json_in["alignments"].toArray()};
        for (const auto& i : temp) {
            attrib_temp.set_avt(i.toString());
            alignments.push_back(attrib_temp);
        }
    } break;
    case sexes_char: {
        auto temp {json_in["sexes"].toArray()};
        for (const auto& i : temp) {
            attrib_temp.set_avt(i.toString());
            sexes.push_back(attrib_temp);
        }
    } break;
    case eye_colors_char:
        break;
    case hair_colors_char:
        break;
    case skin_colors_char:
        break;
    case gods_char: {
        QJsonObject deity_list {json_in["deities"].toObject()};
        for (const auto& i : deity_list.keys()) {
            my_dnd::Gods temp;
            temp.god_name = i;
            temp.alignment = json_in["deities"][i]["alignment"].toString();

            auto class_array {json_in["deities"][i]["class"].toArray()};
            for (const auto& j : class_array)
                temp.class_restrictions.push_back(j.toString());
            auto race_array {json_in["deities"][i]["class"].toArray()};
            for (const auto& j : race_array)
                temp.race_restrictions.push_back(j.toString());
            gods.push_back(temp);
        }
    } break;
    }
}
}    // namespace mydnd_character
