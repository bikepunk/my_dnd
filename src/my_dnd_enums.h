/// \file my_dnd_enums.h
/// \brief enums used in the program
/// \page page1_enums Use of Enums
/// \tableofcontents
/// \section Overview_enums Overview
/// Enums used when accessing various items
/// \subsection name_attributes name_attributes
/// name_attributes enums are used when accessing the various attributes of the
/// information classes for characters (playable and not), monsters, spells, and so
/// on.
/// \subsection file_enum File enumerations
/// Enumeration used for file operations.
///

#ifndef MY_DND_MY_DND_ENUMS_H
#define MY_DND_MY_DND_ENUMS_H

namespace my_dnd {
/// \defgroup my_dnd_enums Enumerations
/// @{

/// Files that are read in.
/// Each should correspond to a json type and a name
enum class File_types
{
    character_attributes,    ///< Includes all race, class, and misc attributes.
    monster_info,    ///< Will be all monster info in one monster file
    playable_characters,    ///< Saved PCs
    spell_info,    ///< All spells
    weapons_info,    ///< All weapons
    random_names,
    class_features
};

/// Attributes used to access items in the various information classes.
enum class Class_attributes
{
    class_attrib,
    align_restriction_attrib,
    hit_die_attrib,
    deity_restriction_attrib,
    class_description_attrib,
    class_brief_attrib,
    damage_reduction_attrib,
    base_skills_points_attrib,
    bonus_feat_attrib,
    bonus_feat_level_up_pattern_attrib,
    bonus_feat_limit_attrib
};

/// Race specific attributes.
enum class Race_attributes
{
    race_attrib,
    favored_attrib,
    addl_lang_attrib,
    creat_size_attrib,
    str_adj_attrib,
    dex_adj_attrib,
    con_adj_attrib,
    itl_adj_attrib,
    wis_adj_attrib,
    cha_adj_attrib,
    land_speed_attrib,
    extra_feat_attrib,
    no_armor_speed_reduction_attrib,
    darkvision_attrib,
    stonecunning_attrib,
    low_light_vision_attrib,
    small_size_bonus_attrib,
    weap_fam_attrib,
    creat_bonus_attrib,
    giant_bonus_attrib,
    stability_bonus_attrib,
    personality_attrib,
    personality_brief_attrib
};

/// Not part of permanent race, used for creations.  List limits of each attribute or
/// additions, or die roll.
enum class Race_misc_attributes
{
    race_addition,
    min_age_val,
    tier1_age_mod_val,
    tier2_age_mod_val,
    tier3_age_mod_val,
    base_height_val,
    base_weight_val,
    height_mod_val,
    weight_mod_val,
    male_height_mod_val,
    male_weight_mod_val
};

/// The saved characters values.
/// race and rate will match to the race and class for the rest of the attributes.
enum class Character_attributes
{
    name_char,
    id_char,
    race_char,
    class_type_char,
    level_char,
    deity_char,
    age_char,
    gender_char,
    height_char,
    weight_char,
    alignment_char,
    str_char,
    dex_char,
    con_char,
    itl_char,
    wis_char,
    cha_char
};

/// Part of character creation.  List limits of each attribute.
/// As part of player character, is part of character.
enum class Character_misc_attributes
{
    alignment_char,
    sexes_char,
    eye_colors_char,
    hair_colors_char,
    skin_colors_char,
    gods_char,
    gold_char
};

/// Attributes used to access items in the various information classes.
enum class Weapon_attributes
{
    weapon_name,
    weapon_proficiency,
    weapon_encumbrance,
    weapon_usefulness,
    weapon_cost,
    weapon_damage_small,
    weapon_damage_medium,
    weapon_critical,
    weapon_weight,
    weapon_type,
    weapon_range_increment
};

enum class Class_features_attributes
{
    feature_flag,
    feature_name,
    feature_bonus,
    feature_start_level,
    feature_type
};

/// @}
}    // namespace my_dnd
#endif    // MY_DND_MY_DND_ENUMS_H
