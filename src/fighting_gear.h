#ifndef ARMOR_INFORMATION_H
#define ARMOR_INFORMATION_H
#include "my_dnd.h"

#include <QString>

namespace mydnd_combat_gear {

////////////////////////////////////////////////////////////////////////////////
/// forwards
////////////////////////////////////////////////////////////////////////////////

class Weapon_information;

////////////////////////////////////////////////////////////////////////////////
/// externs
////////////////////////////////////////////////////////////////////////////////

extern int read_in_json(std::vector<Weapon_information>&);


////////////////////////////////////////////////////////////////////////////////
/// objects
////////////////////////////////////////////////////////////////////////////////

///
/// The Information classes contain all attributes specific to a weapon.
/// All weapon attributes are here.
///
class Weapon_information
{
public:
    explicit Weapon_information();
    // Gets
    my_dnd::Attrib_value_type get_weapon_attribute(
        const my_dnd::Weapon_attributes&) const;
    QString get_weapon_name() const    /// Keep simple, but can use the regular get.
    {
        return weapon.as_string;
    }

    // Sets
    void set_attribute_from_json(const QJsonObject&,
                                 const my_dnd::Weapon_attributes&);
    void set_weapon_name(
        const QString& set_weapon_name)    /// Set here, then populate.
    {
        weapon.set_avt(set_weapon_name);
    }

private:
    void set_attribute(const QString&, const my_dnd::Weapon_attributes&);
    void set_attribute(const int, const my_dnd::Weapon_attributes&);

    /// \name Weapon_information_variables
    ///@{

    my_dnd::Attrib_value_type weapon;
    my_dnd::Attrib_value_type cost;
    my_dnd::Attrib_value_type proficiency;
    my_dnd::Attrib_value_type encumbrance;
    my_dnd::Attrib_value_type usefulness;
    my_dnd::Attrib_value_type damage_small;
    my_dnd::Attrib_value_type damage_medium;
    my_dnd::Attrib_value_type critical;
    my_dnd::Attrib_value_type weight;
    my_dnd::Attrib_value_type type;
    my_dnd::Attrib_value_type range_increment;

    ///@}
};

///
/// Vector of weapon information.
/// Call with [] to get a particular weapon information.
/// use \ref get_weapon_data for a vector of all \ref Weapon_information
///
class Weapon_standard
{
public:
    explicit Weapon_standard();

    Weapon_information& operator[](const QString&);

    std::vector<Weapon_information> get_weapon_data()
    {
        return weapon_data;
    }

private:
    std::vector<Weapon_information> weapon_data;
};

class armor_information
{
public:
    armor_information();
};
}    // namespace mydnd_combat_gear

#endif // ARMOR_INFORMATION_H
