#include "combat.h"
namespace mydnd_combat {

Attrib_value_type calculate_armor_class(
    const mydnd_character::Character_information& character_in)
{
    auto ac_data {character_in.get_armor_class()};

    // Get though dexterity bonus and modifications
    auto current_dex {ac_data.dexterity_modifer.as_int};
    auto max_dex {ac_data.armor_dexterity_max.as_int +
                  ac_data.carrying_load_max.as_int};
    if (current_dex > max_dex) {
        current_dex = ((current_dex - max_dex) > 0 ? (current_dex - max_dex) : 0);
    }

    // 10 + armor + shield + dex mod + size
    auto ac_total {current_dex + ac_data.armor_bonus.as_int +
                   ac_data.shield_bonus.as_int + ac_data.size_modifer.as_int +
                   ac_data.base_ac.as_int};
    Attrib_value_type temp;
    temp.set_avt(ac_total);

    return temp;
}

int damage_delt(const mydnd_character::Character_information& character_in,
                QString damage_type)
{
    int damage {0};

    const QString v = "damage_reduction";
    auto temp = character_in;
    if (std::ranges::find(character_in.class_features, v) !=
        character_in.class_features.end()) {
        // if weapon type
        if (std::ranges::find(character_in.dam_redutc.limitations, damage_type) ==
            character_in.class_features.end()) {
            damage += character_in.dam_redutc.level_up_rate;
        }
    }
    return damage > 0 ? damage : 0;
}

int level_up_class_features(
    const mydnd_character::Character_information& character_in)
{

    int new_level {
        character_in.get_attribute(my_dnd::Character_attributes::level_char)
            .as_int};
    int new_rate {0};

    // though true vector.
    if (character_in.dam_redutc.start_level < new_level) {
        return new_rate;
    }
    else if (character_in.dam_redutc.start_level == new_level) {
        new_rate = character_in.dam_redutc.level_up_rate;
    }
    else {
        for (const auto i : character_in.dam_redutc.level_pattern) {
            if (new_level == i)
                new_rate += character_in.dam_redutc.level_up_rate;
        }
    }

    return new_rate;
}
}    // namespace mydnd_combat
