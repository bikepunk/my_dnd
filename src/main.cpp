///
/// \mainpage
/// \section intro_sec Introduction
/// See README
/// - \ref page1_JSON
/// - \ref page2_JSON

#include "../gui/char_gen.h"
#include "../gui/error_and_debug.cpp"
#include "../gui/main_menu.h"
#include "character.h"
#include "die_roll.h"
#include "json_ops.h"
#include "my_dnd.h"
#include "my_dnd_enums.h"

#include <QApplication>
#include <iostream>    //right now debug
#include <memory>
#include <vector>    //vector

/// Entry to create a character.
/// Performs all functions to create a character.  A file will be created that
/// contains the character info. Passing race/class but remove....
int player_character_creation(const QApplication& app_in)
{

    // Start up character generation sheet.
    mydnd_gui::Char_gen new_char_gui;

    // Prep and add character information sheet.
    //    my_dnd::Character_information dummy_character {true};    // Not used, just
    //    dummy.
    // my_dnd::Character_information testing;
    // mydnd_gui::Char_sheet_main f(dummy_character);
    //  a.add_char_sheet(&f);

    // Forms are ready, show and wait.
    new_char_gui.show();

    app_in.exec();
    // Save character if new, else will save as temp.
    mydnd_character::Player_characters pcs;
    if (new_char_gui.get_char_complete()) {
        pcs.add_character(new_char_gui.get_new_character());
        pcs.write_characters();
    }
    // else write temp;

    return 1;
}

int main(int argc, char* argv[])
{
    QApplication app_main(argc, argv);

    // Start up character generation sheet.
    mydnd_gui::Main_menu startup;

    startup.show();

    app_main.exec();

    return 0;
}
