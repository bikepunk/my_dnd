#ifndef MYDND_CHARACTER_CLASS_FEATURES_H
#define MYDND_CHARACTER_CLASS_FEATURES_H

#include "my_dnd.h"

#include <QString>
#include <algorithm>
namespace mydnd_character {
////////////////////////////////////////////////////////////////////////////////
/// usings
////////////////////////////////////////////////////////////////////////////////
using my_dnd::Attrib_value_type;

////////////////////////////////////////////////////////////////////////////////
/// classes
////////////////////////////////////////////////////////////////////////////////

///
/// Use for new character creation.  Will save the flag when the character is created.
///
struct Features_all {
    QString char_class;
    std::vector<QString> feature_flag;
    std::vector<QString> feature_name;
};

///
/// \brief The Feature_value_type struct
///
struct Feature_value_type {
    QString feature_flag;
    Attrib_value_type feature_value;
};

enum class Feature_types
{
    none,
    Extraordinary,
    Spell_like,
    Supernatural
};

enum class Frequency_types
{
    none,
    day,
    hour
};

enum class Level_up_types
{
    self,
    frequency
};

class Feature_vector
{
public:
    bool find(const QString& what)
    {
        for (auto& i : feature_data) {
            if (i.feature_flag.contains(what))
                return true;
        }
        return false;
    }

private:
    std::vector<Feature_value_type> feature_data;
};

///
/// Limits struct
///
struct Frequency_limit {
    Frequency_types limit_type {Frequency_types::none};
    int limit_limit {0};
};

///
/// Common among features
///
struct Feature_base {
    Feature_types type {Feature_types::none};
    Feature_value_type feature_data;
    int start_level {my_dnd::start_level};
    bool level_up {false};
    std::vector<QString> limitations {"none"};
    Frequency_limit frequency;
};

///
/// The Levelable_feature_base struct if a feature is level-able
/// All variables set to defaults here.
///
struct Levelable_feature_base : public Feature_base {
    Levelable_feature_base()
    {
        level_up = true;
    }
    Level_up_types level_type {Level_up_types::self};
    int level_up_rate {my_dnd::start_level};
    std::vector<int> level_pattern;
};

class Damage_reduction : public Levelable_feature_base
{
public:
    Damage_reduction()
    {
        start_level = 7;
        type = Feature_types::Extraordinary;
        feature_data.feature_flag = "damage_reduction";
        feature_data.feature_value.set_avt(1);
        limitations.clear();
        limitations.push_back("spell");
        level_up_rate = 1;
        level_pattern.push_back(10);
        level_pattern.push_back(13);
        level_pattern.push_back(16);
        level_pattern.push_back(19);
    };
};

class Fast_movement : public Feature_base
{
    Fast_movement()
    {
        type = Feature_types::Extraordinary;
        feature_data.feature_flag = "fast_movement";
        feature_data.feature_value.set_avt(10);
        limitations.clear();
        limitations.push_back("heavy_armor");
        limitations.push_back("heavy_load");
        limitations.push_back("air_speed");
        limitations.push_back("water_speed");
    }
};

class Rage_feature : public Levelable_feature_base
{
public:
    Rage_feature()
    {
        type = Feature_types::Extraordinary;
        level_type = Level_up_types::frequency;
        frequency.limit_type = Frequency_types::day;
        frequency.limit_limit = 1;
        feature_data.feature_flag = "rage";
        feature_data.feature_value.set_avt(1);
        level_up_rate = 1;
        level_pattern.push_back(4);
        level_pattern.push_back(8);
        level_pattern.push_back(12);
        level_pattern.push_back(16);
        level_pattern.push_back(20);
    }
};

class Rage_greater_feature : public Rage_feature
{
    Rage_greater_feature()
    {
        start_level = 11;
        level_up = false;
    }
};

class Rage_tireless_feature : public Rage_feature
{
    Rage_tireless_feature()
    {
        start_level = 17;
        level_up = false;
    }
};

class Rage_mighty_feature : public Rage_feature
{
    Rage_mighty_feature()
    {
        start_level = 20;
        level_up = false;
    }
};

}    // namespace mydnd_character

#endif    // CLASS_FEATURES_H
