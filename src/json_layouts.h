#ifndef MY_DND_JSON_LAYOUTS_H
#define MY_DND_JSON_LAYOUTS_H

#include "../gui/error_and_debug.h"
#include "my_dnd_enums.h"

#include <QString>
#include <vector>

namespace my_dnd {

/// \tparam Attribute_layout Used for reading and writing single attributes
/// Any vector or structs are separate
/// \note For a json-array, use v.
template <class T> struct Attribute_layout {
    T which_attrib;
    QString json_key;
    char value_type;    ///< s=string, i=int, b=bool, v=vector, c=special (like
                        ///< abilities)
};

/// Given the key data
/// \returns first: which attribute as QString
/// \returns second: the type (s/i/...)
template <typename T>
std::pair<QString, char> get_key_data(const std::vector<Attribute_layout<T>>& key_in,
                                      const T& which_to_set)
{
    // Find the attribute passed an attribute enum class
    auto t = std::find_if(key_in.begin(), key_in.end(),
                          [&cm = which_to_set](const Attribute_layout<T>& m) -> bool {
                              return cm == m.which_attrib;
                          });

    // Error out if note found: should not happen.
    if (t == std::end(key_in)) {
        mydnd_err::error("get_key not found");
    }

    return std::make_pair(t->json_key, t->value_type);
}

// miscellaneous

// Forward declarations
std::vector<Attribute_layout<Race_attributes>> get_race_json_layout();
std::vector<Attribute_layout<Class_attributes>> get_class_json_layout();
std::vector<Attribute_layout<Race_misc_attributes>>
get_race_miscellaneous_json_layout();
std::vector<Attribute_layout<Character_misc_attributes>>
get_character_miscellaneous_json_layout();
std::vector<Attribute_layout<Character_attributes>>
get_player_character_json_layout();
std::vector<Attribute_layout<Weapon_attributes>> get_weapon_json_layout();
std::vector<Attribute_layout<Class_features_attributes>> get_json__layout();
}    // namespace my_dnd
#endif
