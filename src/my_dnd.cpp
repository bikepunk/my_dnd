///
/// @file my_dnd.cpp
///
/// @brief Has information and class for character attributes, class, and race.
/// Supports new character.
///
/// @author Mike Benton
/// Contact: bikepunk005@hotmail.com
///
///

#include "my_dnd.h"

namespace my_dnd {

///
/// Calculate Modifier, with even/odd as one value have a bit of math.
/// \param ability
///
Attrib_value_type calculate_ability_modifier(Attrib_value_type ability)
{
    Attrib_value_type temp {};

    if (auto ability_as_int {ability.as_int}; is_even(ability_as_int))
        temp.set_avt(ability_as_int / 2 - mod_adjust);
    else
        temp.set_avt((ability_as_int - 1) / 2 - mod_adjust);

    return temp;
}
///
/// No player character can have intelligence below \ref minimum_intelligence
/// \param current_itl
/// \return \ref minimum_intelligence or higher.
///
int minimum_itl_check(int current_itl)
{
    return current_itl < minimum_intelligence ? minimum_intelligence : current_itl;
}

}    // namespace my_dnd
