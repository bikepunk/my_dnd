#include "fighting_gear.h"

namespace mydnd_combat_gear {
armor_information::armor_information()
{
}

Weapon_information::Weapon_information() {};

///
/// Generic get for any weapon.
/// \ref weapon is possible, but use \ref get_weapon_name
/// \param which_to_get
/// \return The attribute requested--only one.
///
my_dnd::Attrib_value_type Weapon_information::get_weapon_attribute(
    const my_dnd::Weapon_attributes& which_to_get) const
{

    switch (which_to_get) {
        using enum my_dnd::Weapon_attributes;
    case weapon_name:
        return weapon;
    case weapon_cost:
        return cost;
    case weapon_proficiency:
        return proficiency;
    case weapon_encumbrance:
        return encumbrance;
    case weapon_usefulness:
        return usefulness;
    case weapon_damage_small:
        return damage_small;
    case weapon_damage_medium:
        return damage_medium;
    case weapon_critical:
        return critical;
    case weapon_weight:
        return weight;
    case weapon_type:
        return type;
    case weapon_range_increment:
        return range_increment;
    }
    return my_dnd::Attrib_value_type();
}

///
/// Populate the various attributes with the JSON data
/// This is called as the setter; however, use \ref set_weapon_name is to set the
/// weapon name.
/// \param json_in A json object for each weapon
/// \param which_to_set the enum of the attribute
/// \note that the briefs are set using the full JSON Array, and treat Array like a
/// vector.
///
void Weapon_information::set_attribute_from_json(
    const QJsonObject& json_in, const my_dnd::Weapon_attributes& which_to_set)
{
    // Static here as will only be used during character creation.
    static auto json_layout {my_dnd::get_weapon_json_layout()};
    auto key {get_key_data<my_dnd::Weapon_attributes>(json_layout, which_to_set)};

    if (key.second == 's')
        set_attribute(json_in[key.first].toString(), which_to_set);
    else if (key.second == 'i')
        set_attribute(json_in[key.first].toInt(), which_to_set);
}

///
/// Generic set_attribute for QString values.
/// \param value_in
/// \param which_to_set
///
void Weapon_information::set_attribute(const QString& value_in,
                                       const my_dnd::Weapon_attributes& which_to_set)
{
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wswitch"
    switch (which_to_set) {
        using enum my_dnd::Weapon_attributes;
    case weapon_name:
        weapon.set_avt(value_in);
        break;
    case weapon_proficiency:
        proficiency.set_avt(value_in);
        break;
    case weapon_encumbrance:
        encumbrance.set_avt(value_in);
        break;
    case weapon_usefulness:
        usefulness.set_avt(value_in);
        break;
    case weapon_damage_small:
        damage_small.set_avt(value_in);
        break;
    case weapon_damage_medium:
        damage_medium.set_avt(value_in);
        break;
    case weapon_critical:
        critical.set_avt(value_in);
        break;
    case weapon_type:
        type.set_avt(value_in);
        break;
    }
}
#pragma clang diagnostic pop

///
/// Generic set_attribute for int values.
/// \param value_in
/// \param which_to_set
///
void Weapon_information::set_attribute(const int value_in,
                                       const my_dnd::Weapon_attributes& which_to_set)
{
    switch (which_to_set) {
        using enum my_dnd::Weapon_attributes;
    case weapon_cost:
        cost.set_avt(value_in);
        break;
    case weapon_weight:
        weight.set_avt(value_in);
        break;
    case weapon_range_increment:
        range_increment.set_avt(value_in);
        break;
    default:
        mydnd_err::error("Invalid int attribute in Weapon");
    }
}

///
/// Contains all the information on weapon.
/// Call is simply using [weapon] to get information about a weapon.
//\ref race_list is a list of races for use in selectors.
// \ref race_data is a vector of \ref Race_information for each race.
///
Weapon_standard::Weapon_standard()
{
    // Only do once, clear out old data before generating again.
    weapon_data.clear();

    // First step is populating the race data from JSON.

    // \note \ref read_in_json calls \ref Race_information to complete \ref
    /// race_data.
    read_in_json(weapon_data);

    // Put the race_data into the list.
    //    std::transform(
    //        race_data.begin(), race_data.end(), std::back_inserter(race_list),
    //        [](Weapon_information i) {
    //            return
    //            i.get_weapon_attribute(Weapon_attributes::weapon_name).as_string;
    //        });
}

///
/// Weapon_information::operator []
/// \param race_in
/// \return the \ref Weapon_information of the race_in.
///
Weapon_information& Weapon_standard::operator[](const QString& race_in)
{
    // Find the attribute passed an attribute enum class
    auto t = std::find_if(weapon_data.begin(), weapon_data.end(),
                          [&cm = race_in](const Weapon_information& m) -> bool {
                              if (QString::compare(cm, m.get_weapon_name()) == 0)
                                  return true;
                              return false;
                          });
    if (t == std::end(weapon_data))
        mydnd_err::error("Not a valid weapon in Weapon_standard::weapon[]");

    return *t;
}

}    // namespace mydnd_combat_gear
