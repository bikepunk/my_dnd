///
/// @file die_roll.cpp
/// @author Mike H Benton (bikepunk005@hotmail.com)
/// @brief Functions for die roll
/// @version 0.1
/// @date 2021-12-17
///
/// @copyright Copyright (c) 2021
///

#include "die_roll.h"

#include <charconv>
#include <iostream>
#include <string_view>
#include <system_error>

namespace my_dnd {

///
/// Roll a die and return results
/// \param roll_in type of roll
/// \return die_type of the roll
/// \note No negative returns
/// \throw roll_in not valid (w/in range and populated)
///
die_type die_roll(const QString& roll_in)
{

    //  Check valid roll_in from size to having d
    if (roll_in.isEmpty())
        mydnd_err::error("No roll_in value for die_roll");
    const auto roll_length = roll_in.length();
    if (roll_length < d_minimum_length)
        mydnd_err::error("Too few arguments for die_roll", roll_in.length());

    // d_found and verify.
    const auto d_found {roll_in.indexOf(die_marker)};
    if (d_found == -1 or d_found == 0)    // Magic from QT Manual and not start.
        mydnd_err::error("No 'd' in die_roll");

    // Format seems ok, now try to get the values.
    // Checks done by \ref Die_roll
    auto num_ro {roll_in.first(d_found)};
    die_type number_rolls {num_ro.toInt()};
    auto num_sides {roll_in.last(roll_in.length() - d_found - d_length)};
    die_type die_sides {num_sides.toInt()};

    Die_roll a {die_sides};

    return a.roll(number_rolls);
}

/// Validate number of sides.  \n
/// \ref min_die_size \n
/// \ref max_die_size \n
/// \exception sides If less than min or max, will throw. \n
inline void validate_sides(die_type sides)
{
    // Just the common sense check.
    if (sides <= max_die_size && sides >= min_die_size)
        return;

    mydnd_err::error("Invalid die sides", sides);
}

/// Validate the number of rolls.\n
/// \param rolls is number or rolls
/// \throw rolls if less than min_rolls or greater than max_rolls
inline void validate_rolls(die_type rolls)
{
    // Just the commoner sense check.
    if (rolls >= min_rolls && rolls <= max_rolls)
        return;

    mydnd_err::error("Invalid rolls:", rolls);
}

Die_roll::Die_roll(die_type sides): s(sides)
{
    validate_sides(sides);
}

///  Given number of rolls > 0,
///  Roll the dice rolls times and return sum.  Each call verifies the die is
///  correct.
/// \param rolls is the number of rolls to perform.
/// \return  sum of rolls
die_type Die_roll::roll(die_type rolls)
{
    validate_sides(s);
    validate_rolls(rolls);

    die_type roll_results {0};

    std::uniform_int_distribution<> dist(z, s);
    for (die_type i = 0; i < rolls; ++i) {
        roll_results += dist(rd);
    }
    return roll_results;
}

/// Change starting low on die from 1 to new_start.
/// \note The number of sides internal value is reset.  For example, if a 20
/// sided die was created, but a 0 start is needed (d 0 to 19), the number of
/// sides will be reset to match.
/// \param new_start is the new starting for the rand number.
void Die_roll::change_start(die_type new_start)
{
    z = new_start;

    s += new_start;
    validate_sides(s);
}

///
/// Call simply to get a character id.
/// \return a QString of a valid character id.
///
QString character_id_generator()
{
    std::random_device rd;
    std::uniform_int_distribution<> alpha_dist(0, 25);
    std::uniform_int_distribution<> num_dist(0, 9);
    auto character_id_len_sub {10};
    QString temp;
    QString temp2;
    for (auto i = 0; i != character_id_len_sub; ++i) {
        char ch = 'A' + alpha_dist(rd);
        temp += ch;
        temp2 += QString::number(num_dist(rd));
    }

    return temp + temp2;
}

///
/// Returns a random QString from a random QJsonArray
/// \param array_in
/// \return random value from array or "" if array empty.
///
QString get_random_from_array(const QJsonArray& array_in)
{
    if (auto array_data {array_in.size()}; array_data > 0) {
        std::random_device rd;
        std::uniform_int_distribution<> array_dist(0, (array_data - 1));

        return array_in.at(array_dist(rd)).toString();
    }
    return "";
}

/// Rolls and removes the lowest roll.
/// \param number_rolls number of rolls to perform
/// \param sides the number of sides on a die
/// \returns the sum w/o the lowest roll.
/// \note if rolls below \ref min_rolls returns 0.
die_type roll_drop_lowest(die_type number_rolls, die_type sides)
{
    if (number_rolls < min_rolls)
        return 0;

    die_type min_value {sides};
    die_type sum_rolls {0};

    Die_roll temp_die(sides);    // Helper function, so just create temp die.

    for (die_type i = 0; i < number_rolls; ++i) {
        die_type x {temp_die.roll()};
        sum_rolls += x;
        if (x < min_value)
            min_value = x;
    }
    return sum_rolls - min_value;
}

}    // namespace my_dnd
