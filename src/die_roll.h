///
/// @file die_roll.h
/// @author Mike H Benton (bikepunk005@hotmail.com)
/// @brief Rolls dice
/// @version 0.1
/// @date 2021-12-17
///
/// @copyright Copyright (c) 2021
///

#ifndef MY_DND_DIE_ROLL_H
#define MY_DND_DIE_ROLL_H

#include "../gui/error_and_debug.h"

#include <QJsonArray>
#include <QString>
#include <random>    //the rand numbers

namespace my_dnd {

/// Format for die is "#d#" format
const inline QString die_marker {'d'};
const inline auto d_length {die_marker.length()};

/// Min Die length with "#d#" format
const inline auto d_minimum_length {3};

/// Not for use for large data.
/// \note If large numbers, then change; however, overflow would need checked.
/// \warning A lot of other types use die_type.  This is the base type!
using die_type = int;

// Non die
QString character_id_generator();
// Basic die setup.

/// Six sides is standard
static constexpr die_type default_die_side {6};

/// Rolls typically start at one.
static constexpr die_type default_die_start {1};

/// Set default roll to one roll
static constexpr die_type default_number_of_rolls {1};
static constexpr die_type max_rolls {100};
static constexpr die_type min_rolls {1};
static constexpr die_type min_die_size {1};

/// 100 seems good.
constexpr die_type max_die_size {100};

die_type die_roll(const QString& roll_in);

die_type roll_drop_lowest(die_type, die_type sides = default_die_side);

QString get_random_from_array(const QJsonArray&);

/// Creates a die.
/// Each object is one die.
/// Supply the number of  sides, or else default to 1 to 6.
/// Default starting is 1, but can be changed with \ref change_start().
/// Use \ref die_type
class Die_roll
{

public:
    explicit Die_roll(die_type sides);
    die_type roll(die_type rolls = default_number_of_rolls);
    void change_start(die_type new_start);

    ///@{ \name Deleted
    // No copy or moves, do default constructor
    Die_roll() = delete;
    Die_roll(Die_roll const&) = delete;
    Die_roll& operator=(const Die_roll&) = delete;
    Die_roll& operator=(const Die_roll&&) = delete;
    Die_roll(Die_roll&&) = delete;
    Die_roll(Die_roll&) = delete;
    ///@}

private:
    std::random_device rd;    ///< Using hardware if can!
    die_type z {default_die_start};    ///< z is za lowest end of dice
    die_type s {default_die_side};    ///< the number of sides  sides
};
}    // namespace my_dnd

#endif
