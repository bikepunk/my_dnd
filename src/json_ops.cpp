#include "json_ops.h"

namespace mydnd_json_common {

/// Returns the file name based on the file type
/// \param file_type is an enum \ref my_dnd::File_types
/// \return get_filename
QString get_filename(const my_dnd::File_types& file_type)
{
    switch (file_type) {
        using enum my_dnd::File_types;
    case character_attributes:
        return my_dnd::file_loc + my_dnd::character_attributes_file;
    case monster_info:
        return my_dnd::file_loc + my_dnd::monster_file;
    case playable_characters:
        return my_dnd::file_loc + my_dnd::character_data_file;
    case spell_info:
        break;    // TODO add
    case weapons_info:
        return my_dnd::file_loc + my_dnd::weapon_data_file;
    case random_names:
        return my_dnd::file_loc + my_dnd::random_name_file;
    case class_features:
        return my_dnd::file_loc + my_dnd::class_features_file;
    }
    // TODO throw after a file dialog.
    return "error";
}

/// Populates root
/// \param file_name is full path and name
/// \return QByteArray of the root
/// \throw file_name cannot open
/// \throw Json::Value cannot parse
QByteArray get_json_file(const QString& file_name)
{
    QFile input_file(file_name);
    if (!input_file.open(QIODevice::ReadOnly)) {
        mydnd_err::error("Cannot open file in read_json");
    }

    QByteArray root = input_file.readAll();

    input_file.close();

    return root;
}

/// Simple checks of null and if param is object
/// \param doc_in is a QJsonDocument
/// \throws if doc_in is null or not an object
void check_json_doc(const QJsonDocument& doc_in)
{
    if (doc_in.isNull()) {
        mydnd_err::error("Failed to create JSON doc.");
    }
    if (!doc_in.isObject()) {
        mydnd_err::error("JSON is not an object.");
    }
}

/// Simple check if empty
/// \param obj_in is a QJsonObject
/// \throw if empty
void check_json_object(const QJsonObject& obj_in)
{
    if (obj_in.isEmpty()) {
        mydnd_err::error("JSON object is empty.");
    }
}

/// Call to get the root for work.
/// This is the overall root of a file, filter down as needed.
/// \param to_get The type of file as \ref my_dnd::File_types
/// \return An QByteArray of the root
QByteArray get_json_root(const my_dnd::File_types& to_get)
{
    QString file_to_get {get_filename(to_get)};
    return get_json_file(file_to_get);
}

///
/// \brief Prepare the root
/// \param which_root
/// \return an object of the type
/// \note If the switch doesn't have the \ref my_dnd::File_types, then the root is
/// returned as is, so local filter.  This is handy for a json file with a lot of data
/// such as character.
///
QJsonObject prepare_root(const my_dnd::File_types& which_root)
{
    // Get the root data.
    QByteArray root_in {get_json_root(which_root)};

    // Put into doc and verify
    auto root_doc {QJsonDocument::fromJson(root_in)};
    check_json_doc(root_doc);

    // Doc is valid, put into object
    auto root_obj {root_doc.object()};
    check_json_object(root_obj);

    // Switch is for filter for specific types if needed.
    switch (which_root) {
        using enum my_dnd::File_types;
    case playable_characters:
        return root_obj.find(my_dnd::playable_characters_key)->toObject();
    case weapons_info:
        return root_obj.find(my_dnd::weapons_key)->toObject();
    case character_attributes:

    default:
        return root_obj;
    }
    return root_obj;
}
}    // namespace mydnd_json_common

namespace mydnd_combat_gear {

///
/// Get weapon information
/// \return 1 if good, but not used yet.
///
int read_in_json(std::vector<Weapon_information>& data_in)
{
    using namespace mydnd_json_common;
    // Create and get root.
    // Weapon_attributes
    QJsonObject weapons {prepare_root(my_dnd::File_types::weapons_info)};

    //  Cycle through misc items and populate the vectors.
    for (const auto& weapon_to_get : weapons.keys()) {
        Weapon_information temp {};

        // Got the weapon, get the key in each item.
        auto item {weapons.find(weapon_to_get)->toObject()};
        for (const auto& i : my_dnd::get_weapon_json_layout()) {
            temp.set_attribute_from_json(item, i.which_attrib);
        }

        // Set here else will get cleared out.
        temp.set_weapon_name(weapon_to_get);
        data_in.push_back(temp);
    }
    return 0;
}

}    // namespace mydnd_combat_gear

namespace mydnd_random_name {

///
/// \param obj_in Json object of race specific names
/// \param sex_in is null or not a valid sex, see note.
/// \return a name.
/// \note Will return Bob or Jane if not found and Jane, and Jane if sex not Male.
///
QString get_a_name(const QJsonObject& obj_in, const QString& sex_in)
{
    auto sex_to_get = sex_in.isEmpty() ? male_flag : sex_in;
    QString return_name {my_dnd::get_random_from_array(obj_in[sex_to_get].toArray())};
    if (return_name.isEmpty())
        return_name = sex_in == male_flag ? default_male_name : default_female_name;
    return_name = return_name.trimmed();
    return return_name;
}

///
/// Reads in JSON data and returns a name
/// \param race_in
/// \param sex_in (Male, Female).  A few have Neutral.
/// \return A random character name.
///
QString generate_name(const QString& race_in, const QString& sex_in)
{
    // Create and get root.
    using namespace mydnd_json_common;
    QJsonObject root_in {prepare_root(my_dnd::File_types::random_names)};

    //  Get specific race data and send to get name.
    auto race_name_data {root_in.find(race_in)->toObject()};
    QString return_name {get_a_name(race_name_data, sex_in)};

    return return_name;
}

}    // namespace mydnd_random_name

namespace mydnd_character {

/// Plugs and chugs for each race.
/// \param race_in is the used to populate the return value.
/// \param data_in the the QJsonObject.
/// \return a \ref mydnd_character::Race_information object of the race.
Race_information set_character_race_data(const QString& race_in,
                                         const QJsonObject& data_in)
{
    Race_information temp;
    // Get race name first
    // temp.set_string_attribute(race_in, Race_attributes::race_attrib);
    temp.set_race_name(race_in);
    // pairs under race
    auto race_info {data_in.find(race_in)->toObject()};
    using enum my_dnd::Race_attributes;

    for (const auto& i : my_dnd::get_race_json_layout()) {
        temp.set_attribute_from_json(race_info, i.which_attrib);
    }

    return temp;
}

///
/// Reads in character data into data.
/// This will have all data about player playable_characters
/// Different vectors of data will be populated
/// \return 1 if good, but not used yet.
///
int read_in_json(std::vector<Race_information>& data)
{
    // Create and get root.
    // QJsonObject available_races {root_prep_character_attributes(my_dnd::race_key)};
    using namespace mydnd_json_common;
    QJsonObject root_in {prepare_root(my_dnd::File_types::character_attributes)};
    auto available_races {root_in.find(my_dnd::race_key)->toObject()};

    for (const auto& race_name : available_races.keys()) {
        data.push_back(set_character_race_data(race_name, available_races));
    }
    return 0;
}

///
/// Get misc character info used during character generation.
/// is the class to read.
/// \return 1 if good, but not used yet.
///
int read_in_json(Character_misc_standard& data_in)
{
    using namespace mydnd_json_common;
    // Create and get root.
    QJsonObject root_in {prepare_root(my_dnd::File_types::character_attributes)};
    auto available_misc {root_in.find(my_dnd::character_misc_info_key)->toObject()};

    //  Cycle through misc items and populate the vectors.

    for (const auto& misc_item : available_misc.keys()) {

        // Got the item, get the key in each item.
        // auto item {data_in.find(misc_item)->toObject()};
        for (const auto& i : my_dnd::get_character_miscellaneous_json_layout()) {
            if (i.json_key == misc_item)
                data_in.set_vector_attribute(available_misc, i.which_attrib);
        }
    }

    return 0;
}

/// Get misc race information.
///  data
/// \return 1 if good, but not used yet.
int read_in_json(std::vector<Race_additional_information>& data)
{
    using namespace mydnd_json_common;
    // Create and get root.
    // QJsonObject available_races {root_prep_character_attributes(my_dnd::race_key)};
    // Create and get root.
    QJsonObject root_in {prepare_root(my_dnd::File_types::character_attributes)};
    auto available_races {root_in.find(my_dnd::race_key)->toObject()};

    for (const auto& race_name : available_races.keys()) {
        Race_additional_information temp;

        temp.set_race_name(race_name);
        QJsonObject add_race_info {available_races[race_name].toObject()};
        using enum my_dnd::Race_misc_attributes;
        for (const auto& i : my_dnd::get_race_miscellaneous_json_layout()) {
            temp.set_attribute_from_json(add_race_info, i.which_attrib);
        }

        data.push_back(temp);
    }
    return 0;
}

int read_in_json(std::vector<Features_all>& data)
{
    using namespace mydnd_json_common;
    // Create and get root.
    QJsonObject root_in {prepare_root(my_dnd::File_types::class_features)};
    auto available_classes {root_in.find(my_dnd::class_key)->toObject()};
    for (const auto& class_name : available_classes.keys()) {
        Features_all temp;

        temp.char_class = class_name;

        QJsonObject class_info {available_classes[class_name].toObject()};
        for (const auto& i : class_info.keys()) {
            using enum my_dnd::Class_features_attributes;
            temp.feature_flag.push_back(i);

            // Get name for display
            auto feat_name_object = class_info[i].toObject();
            auto feat_name = feat_name_object["name"].toString();
            temp.feature_name.push_back(feat_name);
        }

        data.push_back(temp);
    }
    return 0;
}

/// Reads in all class information.
/// Has information about each class.
/// data a vector of Class_information
/// \return 1 if good, but not implemented
/// \ref Class_information
int read_in_json(std::vector<Class_information>& data)
{
    using namespace mydnd_json_common;
    // Create and get root.
    // QJsonObject available_classes
    // {root_prep_character_attributes(my_dnd::class_key)}; Create and get root.
    QJsonObject root_in {prepare_root(my_dnd::File_types::character_attributes)};
    auto available_classes {root_in.find(my_dnd::class_key)->toObject()};
    for (const auto& class_name : available_classes.keys()) {
        Class_information temp;

        temp.set_class_type_name(class_name);

        QJsonObject class_info {available_classes[class_name].toObject()};
        using enum my_dnd::Class_attributes;
        for (const auto& i : my_dnd::get_class_json_layout()) {
            temp.set_attribute_from_json(class_info, i.which_attrib);
        }
        data.push_back(temp);
    }
    return 0;
}

/// Puts character data into a json root for vectorization
/// \param  character_data A single character
/// \param root_in a temp root to p
void character_to_json(const Character_information& character_data,
                       QJsonObject& root_in)
{

    // used for various values throughout.
    for (auto& i : my_dnd::get_player_character_json_layout()) {

        if (i.value_type == my_dnd::get_type_string)

            root_in[i.json_key] =
                character_data.get_attribute(i.which_attrib).as_string;
        else if (i.value_type == my_dnd::get_type_int)
            root_in[i.json_key] =
                QJsonValue(character_data.get_attribute(i.which_attrib).as_int);
    }
}

/// This will write all characters loaded.
/// New characters will be added if have been added.
/// \return \todo implement value
int write_json(std::vector<Character_information>& characters_data)
{

    // Set up object to populate
    QJsonObject player_object;
    // object to append to player_object of each player characters data

    QJsonObject char_data;
    // Cycle though characters_data and add a character info to char_data
    for (auto& i : characters_data) {

        QJsonObject char_attributes;
        character_to_json(i, char_attributes);

        // Add id
        char_data.insert(
            i.get_attribute(my_dnd::Character_attributes::id_char).as_string,
            char_attributes);
    }
    player_object.insert(my_dnd::playable_characters_key, char_data);

    QJsonDocument player_document {player_object};

    QString file_name {my_dnd::file_loc + my_dnd::character_data_file};
    QFile output_file(file_name);
    if (!output_file.open(QIODevice::ReadWrite)) {
        qCritical("Cannot open file in write_character");
    }

    // Write and close
    output_file.write(player_document.toJson());
    output_file.close();

    return 1;
}

/// Read in the playable characters and added to the Character.
/// characters_data Vector or Character
/// \return \todo Not implemented.
int read_in_json(std::vector<Character_information>& characters_data)
{
    using namespace mydnd_json_common;
    // QJsonObject pc_characters {root_prep_characters()};
    QJsonObject pc_characters {prepare_root(my_dnd::File_types::playable_characters)};
    // For each PC the key is the name get info
    for (const auto& pc_identification : pc_characters.keys()) {
        Character_information temp;

        // Set name and get rest
        temp.set_character_id(pc_identification);

        auto char_info = (pc_characters.find(pc_identification))->toObject();
        for (const auto& i : my_dnd::get_player_character_json_layout()) {

            if (i.value_type == my_dnd::get_type_string) {
                temp.set_attribute_from_json(char_info, i.which_attrib);
            }
            else if (i.value_type == my_dnd::get_type_int)
                temp.set_attribute_from_json(char_info, i.which_attrib);
            else
                mydnd_err::error("Invalid key in PC Characters");
        }

        characters_data.push_back(temp);
    }

    return 0;
}

}    // namespace mydnd_character
