#ifndef MY_DND_MY_DND_H
#define MY_DND_MY_DND_H

#include "../gui/error_and_debug.h"
#include "die_roll.h"
#include "json_layouts.h"
#include "my_dnd_constants.h"
#include "my_dnd_enums.h"

#include <QJsonArray>
#include <QJsonObject>
#include <QString>
#include <algorithm>
#include <iterator>
#include <typeinfo>
#include <utility>
#include <vector>

namespace my_dnd {

///
/// \name HACKS
/// \anchor HACKS
///@{
///
#ifdef _WIN32
inline const QString file_loc {"C:/Users/bikep/source/repos/my_dnd/data/"};
#else
inline const QString file_loc {"/home/mbenton/projects/my_dnd/data/"};
#endif

inline const QString character_attributes_file {"character_dict.json"};
inline const QString monster_file {"monster_dict.json"};
inline const QString character_data_file {"characters.json"};
inline const QString weapon_data_file {"weapons_dict.json"};
inline const QString random_name_file {"random_name_data.json"};
inline const QString class_features_file {"class_features_dict.json"};
/// END HACKS
///@}
///

////////////////////////////////////////////////////////////////////////////////
///  Forward declarations and such
////////////////////////////////////////////////////////////////////////////////

///
/// Simple implementation of is_even.
/// \param  value is an int
/// \return  true=even, false=odd
/// \warning does not throw
///
inline bool is_even(int value) noexcept
{
    return (value % 2 == 0);
}

// Using
using ability_score_type = die_type;    ///< Scores and such go back to die types
using ability_score_string = QString;    ///< We are using QString
using attribute_type = die_type;    ///< Attributes and such are die types

// Class and structs forward declarations
struct Attrib_value_type;

// Function forward declarations
Attrib_value_type calculate_ability_modifier(Attrib_value_type);
int minimum_itl_check(int);

////////////////////////////////////////////////////////////////////////////////
///  Structures and Classes
////////////////////////////////////////////////////////////////////////////////

///
/// Main storage struct for attributes.
/// Holds strings, ints and bools.  To call just do as_...
/// bool, use as_int or as_string with "True" or "False"
/// Use provided setter and getters.
/// For strings, the \ref as_int_default will be default.
/// With a supplied \ref attribute_type and a pre_pend, you can set the string with a
/// prepend value.  For example for a roll of 15 and 16, you can set to 'Roll 1: 15'
/// and 'Roll 2: 16'. with set_avt(15,"Roll 1: ") ...
/// \note \ref as_string will always be \ref attrib_default_string is not set
///
struct Attrib_value_type {
    QString as_string {attrib_default_string};
    attribute_type as_int {as_int_default};

    // set functions, get is just access
    // Sets handle the various changes needed
    void set_avt(const QString& a)
    {
        as_string = a;
    }
    void set_avt(attribute_type a)
    {
        as_string = QString::number(a);
        as_int = a;
    }
    void set_vt(bool a)
    {
        as_int = a;
        if (a)
            as_string = "True";
        else
            as_string = "False";
    }
    void set_avt(attribute_type a, const QString& pre_pend)
    {
        as_string = pre_pend + QString::number(a);
        as_int = a;
    }

    friend bool operator<(const Attrib_value_type& l, const Attrib_value_type& r)
    {
        return l.as_int < r.as_int;
    }
};

///
/// Storage for character multi-pass (name and id)
///
struct Character_name_type {
    Attrib_value_type name;
    Attrib_value_type id;    /// id is 10 upper alpha and 10 numbers (leading 0s).
    friend bool operator<(const Character_name_type& l, const Character_name_type& r)
    {
        return l.name.as_string < l.name.as_string;
    }
};

///
/// POD to hold the gods.
/// For restricting, not listed means any can use.
///
struct Gods {
    std::vector<QString> race_restrictions;    ///< Only races listed can use.
    std::vector<QString> class_restrictions;    ///< Only classes listed can use.
    QString alignment;
    QString god_name;    ///< Given name.
};

///
/// Weapon familiarity for classes.
///
struct Weapon_familiarity {
    Weapon_familiarity(const QString& w, const QString& f):
        weapon {w}, familiarity {f}
    {
    }
    QString weapon;
    QString familiarity;
};

///
/// For bonus fighting certain creature or creature types.
///
struct Creature_bonus {
    Creature_bonus(const QString& c, const int b): creature {c}, bonus {b}
    {
    }
    QString creature;
    int bonus {0};
};

struct Armor_class_data {
    Attrib_value_type base_ac = {.as_string = QString::number(base_armor_class),
                                 .as_int = base_armor_class};
    Attrib_value_type armor_bonus;
    Attrib_value_type shield_bonus;
    Attrib_value_type dexterity_modifer;
    Attrib_value_type size_modifer;
    Attrib_value_type armor_dexterity_max;
    Attrib_value_type carrying_load_max;
    Attrib_value_type total_ac;
};

}    // namespace my_dnd

#endif
