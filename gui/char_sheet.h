#ifndef CHAR_SHEET_H
#define CHAR_SHEET_H

#include "../src/character.h"
#include "../src/my_dnd.h"

#include <QDialog>
#include <QLabel>
#include <qcombobox.h>

namespace Ui {
class Char_sheet;
}

namespace mydnd_gui {

template <typename T>
QString get_attrib(const mydnd_character::Character_information &char_in,
                   const T &which_attrib) {
  return char_in.get_attribute(which_attrib).as_string;
}

class Char_sheet;

void fill_abilities(Char_sheet *, Ui::Char_sheet *,
                    const mydnd_character::Character_information &);

class Char_sheet : public QDialog {
  Q_OBJECT

public:
  explicit Char_sheet(QWidget *parent = nullptr);
  ~Char_sheet();

  void populate_all(const mydnd_character::Character_information &);

  void update_ability(QLabel *, QString);

  void set_tops(const mydnd_character::Character_information &);

  void set_armor_class(const my_dnd::Armor_class_data &);

  mydnd_character::Race_information
  get_race_data(const QString &race_to_get) const {
    return all_race_data[race_to_get];
  };

  mydnd_character::Class_information
  get_class_data(const QString &class_to_get) const {
    return all_class_data[class_to_get];
  };

private:
  Ui::Char_sheet *ui;
  inline static mydnd_character::Race_standard all_race_data;
  inline static mydnd_character::Class_standard all_class_data;
  QPixmap char_picture;
};
} // namespace mydnd_gui

#endif // CHAR_SHEET_H
