///
///@file error_and_debug.h
///@author Mike Benton (bikepunk005@hotmail.com)
///@brief Error functions and any classes
///@version 0.1
///@date 2022-01-05
///
///@copyright Copyright (c) 2022
///

#ifndef MYDND_ERR_ERROR_AND_DEBUG_H
#define MYDND_ERR_ERROR_AND_DEBUG_H

#include <sstream>
#include <stdexcept>
#include <string>

namespace mydnd_err {

/// A simple separator between string and int types.
static inline std::string const err_separator {": "};

/// Error with just a string.
/// \param s a basic error message.
inline void error(const std::string& s)
{
    throw std::runtime_error(s);
}

/// Error with two strings.
/// \param s is concatenated with s2
/// \param s2 is concatenated to s
inline void error(const std::string& s, const std::string& s2)
{
    error(s + s2);
}

/// Error with string and int.
/// \param s is concatenated with : and i
/// \param i is concatenated to s:
/// \ref err_separator
inline void error(const std::string& s, int i)
{
    std::string outz {s + err_separator + std::to_string(i)};
    error(outz);
}
}    // namespace mydnd_err

#endif
