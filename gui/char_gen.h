#ifndef CHAR_GEN_H
#define CHAR_GEN_H

#include "../src/character.h"
#include "../src/my_dnd.h"
#include "char_sheet.h"
#include "gui_values.h"
#include "ui_char_gen.h"

#include <QDialog>
#include <QString>

namespace Ui {
class Char_gen;
}
namespace mydnd_gui {

/// Set drop down with member data
/// \param member is the data (has to have begin/end)
/// \param widget is the drop down
template <class T, class X> void set_dropdowns(T member, X* widget)
{
    for (auto c = 0; auto i : member) {
        widget->addItem(i);
        c++;
    }

    // For combo, set index to 0
    if (QString a {widget->metaObject()->className()}; a.contains("QComboBox"))
        widget->setCurrentIndex(my_dnd::not_found);
}

void set_dropdowns(std::vector<my_dnd::Attrib_value_type>, QComboBox*);

class Char_gen : public QDialog
{
    Q_OBJECT

public:
    explicit Char_gen(QWidget* parent = nullptr);

    void abilities_sort_and_string();
    void pop_rolled();
    void pop_ability_select_groupbox();
    void set_reroll();
    void set_char_sheet();
    void pop_general_char_selectors();
    void update_selected_ability(int, int);
    void reset();
    void update_character();
    void set_update_diety_list();
    void set_update_class_features();

    void update_abilities();
    void check_rolled();

    void do_reroll();
    void s_h_char_sheet();
    void closeEvent(QCloseEvent* event);
    void set_char_complete_widget();

    bool get_char_complete()
    {
        return rca_complete;
    }

    mydnd_character::Character_information get_new_character()
    {
        return new_character;
    }
    ~Char_gen();

private slots:
    void on_selector_race_activated(int);
    void on_selector_class_activated(int);
    void on_selector_alignment_activated(int);
    void on_str_select_activated(int);
    void on_dex_select_activated(int);
    void on_con_select_activated(int);
    void on_itl_select_activated(int);
    void on_cha_select_activated(int);
    void on_wis_select_activated(int);
    void on_reroll_button_clicked();
    void on_reset_button_clicked();
    void on_show_char_sheet_clicked();
    void on_buttonBox_accepted();
    void on_buttonBox_rejected();
    void on_lock_button_clicked();
    void on_selector_sex_activated(int);

    void on_selector_deity_activated(int index);

    void on_selector_deity_textActivated(const QString& arg1);

private:
    // ui
    Ui::Char_gen* ui;

    // TODO: Here? new?
    Char_sheet* char_sheet =
        new Char_sheet();    ///< char_sheet used to display information as
                             ///< character is created.

    /// bools for completions
    bool rca_complete {false};    ///< tracker for when  race/class/abilities done
    bool all_ability_selected {false};
    bool race_selected {false};
    bool class_selected {false};
    bool deity_selected {false};

    // What we are doing.
    mydnd_character::Character_information new_character {true};

    // Needed standards and such
    mydnd_character::Race_standard race_std;
    mydnd_character::Class_standard class_std;
    mydnd_character::Race_additional_standard race_addl;
    mydnd_character::Character_misc_standard misc_info;
    std::vector<mydnd_character::Features_all> feature_data;

    mydnd_character::Ability_type
        rand_sorted;    ///< A copy of original rolled data from new_character
                        ///< constructor and when a reroll is done.  As
                        ///< new_character is updated, data will be read from
                        ///< rand_sorted.

    QString ability_pre {"Roll"};
    QString bonus_spell_pre {"L1 Bonus Spell: "};
    QString skill_points_pre {"Skill Points: "};
    QString feat_points_pre {"Feats: "};
    my_dnd::attribute_type get_ability_from_list(my_dnd::ability_score_type);
    int level_zero {0};
    QString q_zero {"0"};
    void pop_race_adjustments();
};

}    // namespace mydnd_gui

#endif    // CHAR_GEN_H
