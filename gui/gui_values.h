///
///@file gui_values.h
///@author Mike H Benton (bikepunk005@hotmail.com)
///@brief
///@version 0.1
///@date 2022-01-21
///
///@copyright Copyright (c) 2022
///
///

#ifndef MYDND_GUI_GUI_VALUES_H
#define MYDND_GUI_GUI_VALUES_H
#include <QString>

namespace mydnd_gui {

// General widgets abbreviations from char_sheet
inline const QString name_label {"Name"};
inline const QString class_label {"Class"};
inline const QString race_label {"Race"};
inline const QString alingment_label {"Alignment"};
inline const QString diety_label {"Deity"};
inline const QString level_label {"Level"};
inline const QString size_label {"Size"};
inline const QString age_label {"Age"};
inline const QString gender_label {"Gender"};
inline const QString height_label {"Height"};
inline const QString weight_label {"Weight"};

// Armor Class abbreviations
inline const QString ac_abbreviation {"AC"};
inline const QString ac_base_abbreviation {"Base"};
inline const QString ac_armor_bonus_abbreviation {"Armor"};
inline const QString ac_shield_bonus_abbreviation {"Shield"};
inline const QString ac_dex_modifer_abbreviation {"Dex"};
inline const QString ac_size_modifer_abbreviation {"Size"};
inline const QString ac_natural_armor_abbreviation {"Natural"};
inline const QString ac_deflection_modifer_abbreviation {"Deflection"};
inline const QString act_touch_abbreviation {"Touch"};
inline const QString ac_flat_footed_abbreviation {"Flat_foot"};
inline const QString ac_conditionals_abbreviation {"Conditionals"};
}    // namespace mydnd_gui

#endif
