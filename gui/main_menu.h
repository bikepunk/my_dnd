#ifndef MAIN_MENU_H
#define MAIN_MENU_H

#include "char_gen.h"

#include <QDebug>
#include <QDialog>

namespace Ui {
class Main_menu;
}
namespace mydnd_gui {
class Main_menu : public QDialog
{
    Q_OBJECT

public:
    explicit Main_menu(QWidget* parent = nullptr);
    ~Main_menu();

    void new_character()
    {
        mydnd_gui::Char_gen make_new_character(this);    //();//this);
        make_new_character.show();
        this->hide();
        make_new_character.exec();

        // Save character if new, else will save as temp.
        mydnd_character::Player_characters pcs;
        if (make_new_character.get_char_complete()) {
            pcs.add_character(make_new_character.get_new_character());
            pcs.write_characters();
        }
        // Else temp

        // Done, so move on.
        this->show();
    }
private slots:

    void on_new_character_clicked()
    {
        new_character();
    }

private:
    Ui::Main_menu* ui;
};
}    // namespace mydnd_gui
#endif    // MAIN_MENU_H
