#include "main_menu.h"

#include "ui_main_menu.h"

namespace mydnd_gui {
Main_menu::Main_menu(QWidget* parent): QDialog(parent), ui(new Ui::Main_menu)
{
    ui->setupUi(this);
}

Main_menu::~Main_menu()
{
    delete ui;
}

}    // namespace mydnd_gui