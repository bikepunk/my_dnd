///@file char_gen.cpp
///@author  (bikepunk005@yahoo.com)
///@brief The member definitions to create a character.
///@version 0.1
///@date 2021-12-21
///
///@copyright Copyright (c) 2021

#include "char_gen.h"

#include "../src/json_ops.h"

namespace mydnd_gui {

void set_dropdowns(std::vector<my_dnd::Attrib_value_type> member, QComboBox* widget)
{
    for (const auto& i : member) {
        widget->addItem(i.as_string);
    }
    widget->setCurrentIndex(my_dnd::not_found);
}

/// Only constructor used.
/// In essence, creates a new character. The new_char_abilities will be
/// populated upon completion. Race and class info will be used. Final character
/// is saved in a file.
/// abilities. Must have ability_vec populated.
/// \exception if new character (not implemented)
Char_gen::Char_gen(QWidget* parent): QDialog(parent), ui(new Ui::Char_gen)
{
    // QT setup
    ui->setupUi(this);

    // Set position
    this->move(0, 0);

    // Prep and pop abilities
    abilities_sort_and_string();    // Generic new character so no need to roll
                                    // abilities as has been done with class
                                    // initialization.
    pop_rolled();
    pop_ability_select_groupbox();

    // Button work
    set_reroll();

    // Char_sheet set up
    set_char_sheet();

    // The all features, filter later and display
    mydnd_character::read_in_json(feature_data);

    // Hide lock button
    ui->lock_button->setVisible(false);

    // General selectors for a character
    pop_general_char_selectors();
    pop_race_adjustments();
}

mydnd_character::Features_all find_class_feature(
    const std::vector<mydnd_character::Features_all>& class_feat_in,
    const QString& char_class)
{
    auto t = std::find_if(
        class_feat_in.begin(), class_feat_in.end(),
        [&cm = char_class](const mydnd_character::Features_all& m) -> bool {
            if (QString::compare(cm, m.char_class) == 0)
                return true;
            return false;
        });
    if (t == std::end(class_feat_in))
        return mydnd_character::Features_all();
    // mydnd_err::error("Not a valid class in find_class_feature");

    return *t;
}

void Char_gen::set_update_class_features()
{
    QString temp;
    QString char_class = new_character.get_class_type_name();
    mydnd_character::Features_all feats {
        find_class_feature(feature_data, char_class)};

    if (!feats.char_class.isEmpty()) {
        for (const auto& x : feats.feature_name) {
            temp.append(x);
            temp.append('\n');
        }
    }

    ui->class_features->clear();
    ui->class_features->setText(temp);
}

void Char_gen::pop_race_adjustments()
{
    // If no race, no work to do.
    if (race_selected) {
        using enum my_dnd::Race_attributes;
        auto race_data {race_std[new_character.get_race_name()]};

        // Get the mod value for race.
        auto get_race_mod = [&, race_data](const my_dnd::Race_attributes& data) {
            return race_data.get_race_attribute(data).as_string;
        };
        ui->race_race_adjustment_strength->setText(get_race_mod(str_adj_attrib));
        ui->race_race_adjustment_dexterity->setText(get_race_mod(dex_adj_attrib));
        ui->race_race_adjustment_constitution->setText(get_race_mod(con_adj_attrib));
        ui->race_race_adjustment_intelligence->setText(get_race_mod(itl_adj_attrib));
        ui->race_race_adjustment_wisdom->setText(get_race_mod(wis_adj_attrib));
        ui->race_race_adjustment_charisma->setText(get_race_mod(cha_adj_attrib));
    }
    else {
        ui->race_race_adjustment_strength->setText(q_zero);
        ui->race_race_adjustment_dexterity->setText(q_zero);
        ui->race_race_adjustment_constitution->setText(q_zero);
        ui->race_race_adjustment_intelligence->setText(q_zero);
        ui->race_race_adjustment_wisdom->setText(q_zero);
        ui->race_race_adjustment_charisma->setText(q_zero);
    }
}

///
/// Creates sorted list of die rolls for abilities in \ref rand_sorted.
/// This allows easier selection and adds Roll so can see ties.
///
void Char_gen::abilities_sort_and_string()
{
    // Create sorted data
    rand_sorted.clear_data();

    rand_sorted = new_character.get_abilities();
    rand_sorted.str.set_avt(rand_sorted.str.as_int, "Roll 1: ");
    rand_sorted.dex.set_avt(rand_sorted.dex.as_int, "Roll 2: ");
    rand_sorted.con.set_avt(rand_sorted.con.as_int, "Roll 3: ");
    rand_sorted.itl.set_avt(rand_sorted.itl.as_int, "Roll 4: ");
    rand_sorted.wis.set_avt(rand_sorted.wis.as_int, "Roll 5: ");
    rand_sorted.cha.set_avt(rand_sorted.cha.as_int, "Roll 6: ");
}

///
/// Fills the rolled values so the user can see which to pick.
///
void Char_gen::pop_rolled()
{
    ui->roll_1->setText(rand_sorted.str.as_string);
    ui->roll_2->setText(rand_sorted.dex.as_string);
    ui->roll_3->setText(rand_sorted.con.as_string);
    ui->roll_4->setText(rand_sorted.itl.as_string);
    ui->roll_5->setText(rand_sorted.wis.as_string);
    ui->roll_6->setText(rand_sorted.cha.as_string);
}

/// Fills the GroupBox for selection
void Char_gen::pop_ability_select_groupbox()
{
    QList<QComboBox*> ability_scores_selected =
        ui->selected->findChildren<QComboBox*>();

    QList<QLabel*> abilities_rolled = ui->rolled->findChildren<QLabel*>();

    for (auto i : ability_scores_selected) {
        i->clear();
        for (auto x : abilities_rolled)
            i->addItem(x->text());
        i->setCurrentIndex(my_dnd::not_found);
    }
}

///
/// The re-roll only shows if check_ability is true
/// \ref my_dnd::check_ability()
///
void Char_gen::set_reroll()
{
    if (mydnd_character::check_ability(rand_sorted)) {
        ui->reroll_button->hide();
    }
}

///
/// Sets in place and show char_sheet
///
void Char_gen::set_char_sheet()
{
    char_sheet->move(this->width(), 0);
    char_sheet->show();
}

void Char_gen::set_update_diety_list()
{
    // Get new list
    std::vector<QString> diety_list {
        mydnd_character::get_filtered_god_list(misc_info, new_character)};

    // Since new list may have less or more clear out and set selector.
    ui->selector_deity->clear();
    set_dropdowns(diety_list, ui->selector_deity);

    // Get current, set to null, and then set back if can still use based on
    // selections.
    QString current_god {
        new_character.get_attribute(my_dnd::Character_attributes::deity_char)
            .as_string};
    new_character.set_attribute("", my_dnd::Character_attributes::deity_char);
    for (const auto& i : diety_list)
        if (i == current_god) {
            new_character.set_attribute(current_god,
                                        my_dnd::Character_attributes::deity_char);
            ui->selector_deity->setCurrentText(current_god);
        }
    // Check if done
    if (new_character.get_class_type_name() == "Cleric") {
        if (current_god.isEmpty())
            deity_selected = false;
        else
            deity_selected = true;
    }
    else
        deity_selected = true;
}

/// Populates the drop-downs for race and class
void Char_gen::pop_general_char_selectors()
{
    set_dropdowns<std::vector<QString>, QComboBox>(class_std.get_class_list(),
                                                   ui->selector_class);

    set_dropdowns<std::vector<QString>, QComboBox>(race_std.get_race_list(),
                                                   ui->selector_race);
    set_dropdowns(misc_info.get_character_misc_information(
                      my_dnd::Character_misc_attributes::alignment_char),
                  ui->selector_alignment);
    set_dropdowns(misc_info.get_character_misc_information(
                      my_dnd::Character_misc_attributes::sexes_char),
                  ui->selector_sex);
    // first here.  Then with update character.
    set_update_diety_list();
}

///
/// Entry point to update widgets after a callback of an ability selected.
/// Determine which selection changed and with what rolled value.
/// If rolled value was previously selected, then undo for that selection.
/// Grey out associated rolled.
/// Update modifiers.
/// \param selected_roll Index to the roll
/// \param ability_number the ability by number
void Char_gen::update_selected_ability(int selected_roll, int ability_number)
{
    // Nothing to see here
    if (selected_roll == my_dnd::not_found)
        return;

    // Get list of rolled values and abilities that have been selected (or not)
    QList<QLabel*> abilities_rolled = ui->rolled->findChildren<QLabel*>();
    QList<QComboBox*> ability_scores_select =
        ui->selected->findChildren<QComboBox*>();

    // Determine if the selected roll has been previously selected.  If so then clear
    // it (the rolled value) so can be selected again (using not_found).
    if (!abilities_rolled[selected_roll]->isEnabled()) {
        //  Go through each ability selected widget to see if the new selected
        //  value was in a previous widget.  If so, set to not found.
        for (auto i = 0; auto w : ability_scores_select) {
            if (w->currentIndex() == selected_roll && i != ability_number) {
                w->setCurrentIndex(my_dnd::not_found);
            }
            ++i;
        }
    }

    // Take care of gray, and update char sheet.
    check_rolled();

    update_character();
    ui->bonus_spell->setText(
        bonus_spell_pre +
        QString::number(mydnd_character::get_bonus_spell(
            new_character.get_class_type_name(), new_character.get_abilities())));
}

/// Reset selections that are selected for abilities
void Char_gen::reset()
{
    // clear out each selected score
    // Get list of rolled and abilities
    QList<QLabel*> abilities_rolled = ui->rolled->findChildren<QLabel*>();
    QList<QComboBox*> ability_scores_select =
        ui->selected->findChildren<QComboBox*>();

    // Cycle though both widgets and set and new.
    for (auto i : abilities_rolled)
        i->setEnabled(true);
    for (auto i : ability_scores_select)
        i->setCurrentIndex(my_dnd::not_found);
    update_character();

    // Reset indicator
    all_ability_selected = false;
    set_char_complete_widget();
}

/// Any changes update the character and associated sheet.
/// This will be written once Finish Character is selected (if ready)
void Char_gen::update_character()
{
    update_abilities();

    mydnd_character::calculate_vital_statistics(race_addl, new_character);
    set_update_diety_list();
    if (!new_character.get_race_name().isEmpty()) {
        new_character.set_character_name(
            mydnd_random_name::generate_name(new_character.get_race_name(), ""));
        new_character.set_race_data(race_std[new_character.get_race_name()]);
    }
    if (!new_character.get_class_type_name().isEmpty()) {
        new_character.set_class_data(class_std[new_character.get_class_type_name()]);
    }
    new_character.update_modifiers();
    ui->skill_points->setText(
        skill_points_pre +
        QString::number(mydnd_character::calculate_number_new_skills(new_character)));
    ui->feat_points->setText(
        feat_points_pre +
        QString::number(mydnd_character::calculate_number_new_feats(new_character)));
    // Done, update char sheet.
    char_sheet->populate_all(new_character);
}

my_dnd::attribute_type Char_gen::get_ability_from_list(my_dnd::ability_score_type a)
{
    switch (a) {
    case (my_dnd::str_position):
        return rand_sorted.str.as_int;
        break;
    case (my_dnd::dex_position):
        return rand_sorted.dex.as_int;
        break;
    case (my_dnd::con_position):
        return rand_sorted.con.as_int;
        break;
    case (my_dnd::itl_position):
        return rand_sorted.itl.as_int;
        break;
    case (my_dnd::wis_position):
        return rand_sorted.wis.as_int;
        break;
    case (my_dnd::cha_position):
        return rand_sorted.cha.as_int;
        break;
    default:
        return level_zero;    // level_zero;
    }
}
/// Update Character with rolls as selected.
/// Sets to 0 if not indexed
void Char_gen::update_abilities()
{
    mydnd_character::Ability_type temp_abilities;    // TODO Why?
    QList<QComboBox*> ability_scores_selected =
        ui->selected->findChildren<QComboBox*>();

    // Simple to make sure 0 is passed if nothing is set.
    // A is the index.
    auto a {ability_scores_selected[my_dnd::str_position]->currentIndex()};
    auto b = ui->race_race_adjustment_strength->text().toInt();
    new_character.set_attribute(get_ability_from_list(a) + b,
                                my_dnd::Character_attributes::str_char);

    a = ability_scores_selected[my_dnd::con_position]->currentIndex();
    b = ui->race_race_adjustment_constitution->text().toInt();
    new_character.set_attribute(get_ability_from_list(a) + b,
                                my_dnd::Character_attributes::con_char);

    a = ability_scores_selected[my_dnd::dex_position]->currentIndex();
    b = ui->race_race_adjustment_dexterity->text().toInt();
    new_character.set_attribute(get_ability_from_list(a) + b,
                                my_dnd::Character_attributes::dex_char);

    a = ability_scores_selected[my_dnd::itl_position]->currentIndex();
    b = ui->race_race_adjustment_intelligence->text().toInt();
    new_character.set_attribute(get_ability_from_list(a) + b,
                                my_dnd::Character_attributes::itl_char);

    a = ability_scores_selected[my_dnd::wis_position]->currentIndex();
    b = ui->race_race_adjustment_wisdom->text().toInt();
    new_character.set_attribute(get_ability_from_list(a) + b,
                                my_dnd::Character_attributes::wis_char);

    a = ability_scores_selected[my_dnd::cha_position]->currentIndex();
    b = ui->race_race_adjustment_charisma->text().toInt();
    new_character.set_attribute(get_ability_from_list(a) + b,
                                my_dnd::Character_attributes::cha_char);
}

///
/// Make sure rolled are active and deactivate if needed.
/// Sets all rolled as active, then if associated selections has a value,
/// deactivate rolled.
/// Since it works, here will mark \ref all_ability_selected.
///
void Char_gen::check_rolled()
{
    int check_all_selected {0};

    QList<QLabel*> ability_in = ui->rolled->findChildren<QLabel*>();

    QList<QComboBox*> abilities_selected = ui->selected->findChildren<QComboBox*>();

    // Cycle though both.
    for (auto i = 0; auto x : ability_in) {
        x->setDisabled(false);

        for (auto v : abilities_selected)
            if (i == v->currentIndex()) {
                x->setDisabled(true);
                check_all_selected++;
            }

        ++i;
    }

    // Flag set for done!
    if (check_all_selected == my_dnd::num_abilities) {
        all_ability_selected = true;
    }
    else {
        all_ability_selected = false;
    }
    set_char_complete_widget();
}

/// Clear rolled and selections, add new rolled values.
/// Set with reroll_widget, else cannot be done.
/// \note form re-population is where new data will be.
void Char_gen::do_reroll()
{
    // Set up new data, roll, sort
    new_character.roll_new_character();
    // Create rolled data for character selection and sort.

    // Repopulate form
    abilities_sort_and_string();
    pop_rolled();
    pop_ability_select_groupbox();

    // Button work
    set_reroll();
    // pop_ability_select();
    // init_reroll_widgets(new_roll);
    reset();
    update_character();
}

/// Show and hide character sheet.
/// \todo change display button
void Char_gen::s_h_char_sheet()
{
    if (char_sheet->isVisible()) {
        char_sheet->hide();
    }
    else {
        char_sheet->show();
    }
}
void Char_gen::set_char_complete_widget()
{
    // TODO: in new_char char_class
    if (race_selected && class_selected && all_ability_selected && deity_selected) {
        ui->lock_button->setEnabled(true);
        ui->lock_button->setVisible(true);
        ui->lock_button->setText("Lock Selection");
        rca_complete = true;
    }
    else {
        ui->lock_button->setEnabled(false);
        ui->lock_button->setVisible(false);
        rca_complete = false;
    }
}

/// When a race is selected not only this, but char_sheet needs updates.
void Char_gen::on_selector_race_activated(int index)
{
    // Lambda to set various race additional attributes
    auto get_attrib = [&](my_dnd::Race_misc_attributes t) {
        return race_addl.get_race_data()[index].get_race_attribute(t).as_int;
    };

    // Set new race and associated data
    auto new_race {race_std.get_race_list()[index]};    // TODO put below.
    new_character.set_attribute(new_race, my_dnd::Character_attributes::race_char);

    new_character.set_attribute(get_attrib(my_dnd::Race_misc_attributes::min_age_val),
                                my_dnd::Character_attributes::age_char);

    using enum my_dnd::Race_attributes;
    ui->race_text->setText(race_std.get_race_data()[index]
                               .get_race_attribute(personality_attrib)
                               .as_string);
    ui->favored->setText(
        race_std.get_race_data()[index].get_race_attribute(favored_attrib).as_string);

    race_selected = true;
    pop_race_adjustments();
    // Updates vitals and such.
    update_character();

    set_char_complete_widget();
}

void Char_gen::on_selector_class_activated(int index)
{
    // Update the new_character information based on the selection
    new_character.set_attribute(class_std.get_class_list()[index],
                                my_dnd::Character_attributes::class_type_char);

    // Update the ui info based on class
    ui->class_text->setText(
        class_std.get_class_data()[index]
            .get_class_attribute(my_dnd::Class_attributes::class_description_attrib)
            .as_string);

    update_character();

    ui->bonus_spell->setText(
        bonus_spell_pre +
        QString::number(mydnd_character::get_bonus_spell(
            new_character.get_class_type_name(), new_character.get_abilities())));

    class_selected = true;
    set_char_complete_widget();
    set_update_class_features();
}

void Char_gen::on_selector_alignment_activated(int index)
{
    new_character.set_attribute(ui->selector_alignment->currentText(),
                                my_dnd::Character_attributes::alignment_char);
    update_character();
}
void Char_gen::on_selector_sex_activated(int index)
{
    new_character.set_attribute(ui->selector_sex->currentText(),
                                my_dnd::Character_attributes::gender_char);
    update_character();
}
void Char_gen::on_str_select_activated(int index)
{
    update_selected_ability(index, my_dnd::str_position);
}
void Char_gen::on_dex_select_activated(int index)
{
    update_selected_ability(index, my_dnd::dex_position);
}
void Char_gen::on_con_select_activated(int index)
{
    update_selected_ability(index, my_dnd::con_position);
}
void Char_gen::on_itl_select_activated(int index)
{
    update_selected_ability(index, my_dnd::itl_position);
}
void Char_gen::on_cha_select_activated(int index)
{
    update_selected_ability(index, my_dnd::cha_position);
}
void Char_gen::on_wis_select_activated(int index)
{
    update_selected_ability(index, my_dnd::wis_position);
}

void Char_gen::on_reset_button_clicked()
{
    reset();
}

void Char_gen::on_reroll_button_clicked()
{
    do_reroll();
}
void Char_gen::on_show_char_sheet_clicked()
{
    s_h_char_sheet();
}

void Char_gen::closeEvent(QCloseEvent* event)
{
    char_sheet->close();
}
void Char_gen::on_buttonBox_accepted()
{
    char_sheet->close();
    // delete char_sheet;
}
Char_gen::~Char_gen()
{
    delete ui;
}

void Char_gen::on_buttonBox_rejected()
{
    char_sheet->close();
    // delete char_sheet;
}
void Char_gen::on_lock_button_clicked()
{
    if (rca_complete) {
        ui->selector_race->setEnabled(false);
        ui->selector_class->setEnabled(false);
        ui->selected->setEnabled(false);
        ui->lock_button->setEnabled(false);
        ui->lock_button->setText("R.C.A. locked.");
        // ui->selector_race->isEnabled(false);
    }
    // char_sheet->close();
    // this->done(1);
}

void Char_gen::on_selector_deity_activated(int index)
{
}

void Char_gen::on_selector_deity_textActivated(const QString& arg1)
{
    // Update the new_character information based on the selection
    new_character.set_attribute(arg1, my_dnd::Character_attributes::deity_char);

    update_character();
    set_char_complete_widget();
}

}    // namespace mydnd_gui
